class ExternalServices
  def start_docker_containers_if_needed
    return if Rails.env.in?(["production", "canary", "staging"]) # Docker Host is already setup and container running
    return if ENV["CI"] # Gitlab CI starts services (aka docker container) based on .gitlab-ci.yml
    return if ENV["IN_DOCKER"] # Running in Docker (manually defined)

    raise "PLEASE INSTALL docker-compose" unless Kernel.system("which", "docker-compose")

    compose_separator = `docker-compose --version`.match?(/Docker Compose version v2\./) ? "-" : "_"
    compose_project_name = Rails.root.basename.to_s.downcase.shellescape
    container_running = `docker inspect -f '{{.State.Running}}' #{compose_project_name}#{compose_separator}elasticsearch#{compose_separator}1`.strip == "true"
    return if container_running

    puts "Docker container(s) not running. Starting docker containers..."

    # Scale elasticsearch to allow parallel tests to run
    Kernel.system("docker-compose", "up", "-d", "--remove-orphans", "--scale", "elasticsearch=3")
  end
end

# Boot docker container VERY early (i.e. even before fully loading rails app) as these are mere shell scripts (i.e. no ruby class loading issues)
ExternalServices.new.start_docker_containers_if_needed
