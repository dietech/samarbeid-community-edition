class WorkflowSupportException < StandardError; end

class NotImplementedException < StandardError; end
