# Moved to -> https://gitlab.com/samarbeid-org/samarbeid

# samarbeid community edition


## Willkommen

Auf der Webseeite von samarbeid (https://samarbeid.org) findet Ihr alles, was Ihr zu samarbeid wissen müsst.
Abonniert unseren Newsletter und folgt uns auf Twitter (https://twitter.com/samarbeid_org) um auf dem Laufenden zu bleiben.

## Lizenz

Der Quellcode von samarbeid ist unter der in LICENSE abgelegten MIT Open Source Lizenz veröffentlicht.

Samarbeid ist beim DPMA (Deutsche Patent- und Markenamt) als Marke zur Registrierung angemeldet. Jegliche Verwendung der Marke und des Logos von samarbeid - außerhalb der Nutzung der Software - bedarf der schriftlichen Zustimmung durch den Eigentümer. Anfragen hierzu per E-Mail an: kontakt@samarbeid.org
