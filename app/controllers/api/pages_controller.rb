class Api::PagesController < ApiController
  before_action :authenticate_user!, except: :show
  skip_authorization_check only: :show

  def show
    @page = Page.find_by(slug: params[:slug])
  end
end
