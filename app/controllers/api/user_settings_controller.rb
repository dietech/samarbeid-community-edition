class Api::UserSettingsController < ApiController
  load_and_authorize_resource :user, parent: false, class: "User"
  skip_authorize_resource only: [:show]

  def show
    authorize! :show_settings, @user
  end

  def update
    @user.update(update_params)

    api_response_with(view: :show, errors: @user.errors)
  end

  def update_admin_status
    if ActiveRecord::Type::Boolean.new.cast(params.require(:admin))
      @user.update(role: :team_admin)
    else
      @user.update(role: :user)
    end

    api_response_with(view: :show, errors: @user.errors)
  end

  def update_active_status
    if ActiveRecord::Type::Boolean.new.cast(params.require(:active))
      @user.activate
    else
      @user.deactivate
    end

    api_response_with(view: :show, errors: @user.errors)
  end

  def update_password
    if (!current_user.is_admin? || @user == current_user) && !@user.valid_password?(params[:current_password])
      @user.errors.add(:current_password, "ist nicht korrekt")
    end

    bypass_sign_in @user if @user.errors.none? && @user.update(update_password_params) && (@user == current_user)

    api_response_with(view: :show, errors: @user.errors)
  end

  def update_notification_settings
    new_interval = update_notifications_params
    if new_interval.respond_to?(:to_i) && new_interval.to_i == new_interval
      @user.update_columns(noti_interval: new_interval, noti_next_send_at: @user.schedule_next_delivery(new_interval))
    else
      logger.error("FIXME: noti interval change fehler mit isTrusted besteht noch") # FIXME: https://stackoverflow.com/questions/44815172/log-shows-error-object-istrustedtrue-instead-of-actual-error-data ?
    end
    api_response_with(view: :show, errors: @user.errors)
  end

  def groups
    @groups = @user.groups.order(:name)

    api_response_with(view: "api/groups/list")
  end

  private

  wrap_parameters User, include: User.attribute_names + [:password, :password_confirmation, :avatar, :remove_avatar], format: [:json, :multipart_form]

  def update_params
    params.require(:user).permit(:email, :firstname, :lastname, :avatar, :remove_avatar)
  end

  def update_password_params
    params.require(:user).permit(:password, :password_confirmation)
  end

  def update_notifications_params
    params.require(:interval)
  end
end
