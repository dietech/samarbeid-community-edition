class Api::DossierFieldDefinitionsController < ApiController
  load_and_authorize_resource

  def create
    @dossier_definition = DossierDefinition.find(params.require(:dossier_definition_id))
    dossier_field_definition = Interactors::DossierFieldDefinitionInteractor.create(@dossier_definition, dossier_field_definition_params)

    api_response_with(view: "api/dossier_definitions/show", errors: dossier_field_definition.errors)
  end

  def update
    Interactors::DossierFieldDefinitionInteractor.update(@dossier_field_definition, dossier_field_definition_params)

    @dossier_definition = @dossier_field_definition.definition
    api_response_with(view: "api/dossier_definitions/show", errors: @dossier_field_definition.errors)
  end

  def destroy
    Interactors::DossierFieldDefinitionInteractor.destroy(@dossier_field_definition)

    @dossier_definition = @dossier_field_definition.definition.reload
    api_response_with(view: "api/dossier_definitions/show", errors: @dossier_field_definition.errors)
  end

  def move
    Interactors::DossierFieldDefinitionInteractor.move(@dossier_field_definition, params.require(:index).to_i)

    @dossier_definition = @dossier_field_definition.definition.reload
    api_response_with(view: "api/dossier_definitions/show", errors: @dossier_field_definition.errors)
  end

  private

  def dossier_field_definition_params
    params.permit(:name, :required, :recommended, :unique, :content_type, options: {})
  end
end
