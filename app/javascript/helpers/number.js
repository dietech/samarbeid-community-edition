const currentLocale = 'de-DE'

export function number2LocaleString (n, options = undefined) {
  return n.toLocaleString(currentLocale, options)
}
