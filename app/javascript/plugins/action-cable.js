import Vue from 'vue'
import ActionCableVue from 'actioncable-vue'

const connectionUrl = document.head.querySelector("meta[name='action-cable-url']")?.content

if (connectionUrl) {
  Vue.use(ActionCableVue, {
    debug: true,
    debugLevel: 'error',
    connectionUrl: connectionUrl,
    connectImmediately: true
  })
}
