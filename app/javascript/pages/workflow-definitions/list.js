import ListPage from '../list-page'
import ListContent from './list-content'

export default {
  name: 'WorkflowDefinitionListPage',
  mixins: [ListPage],
  data () {
    return {
      filter: {
        page: 1,
        queryText: null,
        order: 'name_asc'
      }
    }
  },
  computed: {
    pageTitleParts () {
      return ['Maßnahmevorlagen']
    },
    pageContentComponents () {
      return ListContent
    }
  },
  methods: {
    initPageRequestUrl () {
      return this.$apiEndpoints.workflowDefinitions.index()
    },
    initPageRequestParams () {
      return {
        page: this.pages.current,
        query: this.filters.values.query,
        order: this.filters.values.order
      }
    },
    onPropUpdated (prop, value, info) {
      ListPage.methods.onPropUpdated.call(this, prop, value, info)
      this.$router.replace({ name: 'workflow-definitions', query: this.createQuery() })
    },
    filterFields () {
      return [
        [
          {
            name: 'query',
            type: 'text',
            label: 'Maßnahmevorlagentitel',
            default: ''
          },
          {
            name: 'order',
            type: 'single-select',
            label: 'Sortierung',
            items: [
              { text: 'Name – aufsteigend', value: 'name_asc' },
              { text: 'Name – absteigend', value: 'name_desc' },
              { text: 'Erstellungsdatum – neueste zuerst', value: 'created_at_desc' },
              { text: 'Erstellungsdatum – älteste zuerst', value: 'created_at_asc' }
            ],
            default: 'name_asc',
            cols: 4
          }]
      ]
    }
  }
}
