import Page from '../page'
import ProfileContent from './profile-content'
import User from 'mixins/models/user'

export default {
  name: 'UserProfilePage',
  mixins: [Page, User],
  props: {
    id: {
      type: Number,
      required: true
    }
  },
  computed: {
    pageTitleParts () {
      return [...(this.value ? [this.userFullnameFor(this.value)] : []), 'Nutzer Profil']
    },
    pageContentComponents () {
      return ProfileContent
    }
  },
  methods: {
    initPageRequestUrl () {
      return this.$apiEndpoints.users.show(this.id)
    }
  }
}
