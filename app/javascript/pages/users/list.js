import ListPage from '../list-page'
import ListContent from './list-content'

export default {
  name: 'UserListPage',
  mixins: [ListPage],
  computed: {
    pageTitleParts () {
      return ['Nutzer']
    },
    pageContentComponents () {
      return ListContent
    }
  },
  methods: {
    initPageRequestUrl () {
      return this.$apiEndpoints.users.index()
    },
    initPageRequestParams () {
      return {
        page: this.pages.current,
        query: this.filters.values.query,
        order: this.filters.values.order,
        tab_category: this.filters.values.tab_category
      }
    },
    onPropUpdated (prop, value, info) {
      ListPage.methods.onPropUpdated.call(this, prop, value, info)
      this.$router.replace({ name: 'users', query: this.createQuery() })
    },
    filterTabs () {
      return {
        name: 'tab_category',
        items: [
          { text: 'Alle', value: 'ALL' },
          { text: 'Standardnutzer', value: 'NO_ADMIN' },
          { text: 'Admins', value: 'ADMIN' }
        ],
        counts: this.valueAttributeOrDefault('tab_categories', {}),
        default: 'ALL'
      }
    },
    filterFields () {
      return [
        [
          {
            name: 'query',
            type: 'text',
            label: 'Nutzername / E-Mail',
            default: ''
          },
          {
            name: 'order',
            type: 'single-select',
            label: 'Sortierung',
            items: [
              { text: 'Name – aufsteigend', value: 'name_asc' },
              { text: 'Name – absteigend', value: 'name_desc' },
              { text: 'Erstellungsdatum – neueste zuerst', value: 'created_at_desc' },
              { text: 'Erstellungsdatum – älteste zuerst', value: 'created_at_asc' }
            ],
            default: 'name_asc',
            cols: 4
          }]
      ]
    }
  }
}
