import ListPage from '../list-page'
import ListContent from './list-content'
import map from 'lodash/map'
import parseInt from 'lodash/parseInt'

export default {
  name: 'TaskListPage',
  mixins: [ListPage],
  computed: {
    pageTitleParts () {
      return ['Aufgaben']
    },
    pageContentComponents () {
      return ListContent
    }
  },
  methods: {
    initPageRequestUrl () {
      return this.$apiEndpoints.tasks.index()
    },
    initPageRequestParams () {
      return {
        page: this.pages.current,
        order: this.filters.values.order,
        tab_category: this.filters.values.tab_category,
        assignee_ids: this.filters.values.assignee_ids,
        contributor_ids: this.filters.values.contributor_ids,
        workflow_assignee_ids: this.filters.values.workflow_assignee_ids,
        workflow_definition_ids: this.filters.values.workflow_definition_ids,
        task_definition_ids: this.filters.values.task_definition_ids,
        ambition_ids: this.filters.values.ambition_ids
      }
    },
    onPropUpdated (prop, value, info) {
      ListPage.methods.onPropUpdated.call(this, prop, value, info)
      this.$router.replace({ name: 'tasks', query: this.createQuery() })
    },
    filterTabs () {
      return {
        name: 'tab_category',
        items: [
          { text: 'Todo', value: 'TODO' },
          { text: 'Fällig', value: 'DUE' },
          { text: 'Meine Merkliste', value: 'MARKED' },
          { text: 'Alle', value: 'ALL' }
        ],
        counts: this.valueAttributeOrDefault('tab_categories', {}),
        default: 'TODO'
      }
    },
    filterFields () {
      return [
        [
          {
            name: 'task_definition_ids',
            type: 'multi-select',
            label: 'Art der Aufgabe',
            items: this.valueAttributeOrDefault('task_definitions'),
            default: [],
            cast: (value) => map([value].flat(), parseInt)
          },
          {
            name: 'order',
            type: 'single-select',
            label: 'Sortierung',
            items: [
              { text: 'Erstellungsdatum – neueste zuerst', value: 'created_at_desc' },
              { text: 'Erstellungsdatum – älteste zuerst', value: 'created_at_asc' },
              { text: 'Titel – aufsteigend', value: 'title_asc' },
              { text: 'Titel – absteigend', value: 'title_desc' },
              { text: 'Fälligkeit – nach Dringlichkeit', value: 'by_due_date' }
            ],
            default: 'created_at_desc',
            cols: 4
          }],
        [
          {
            name: 'assignee_ids',
            type: 'user-multi-select',
            label: 'Verantwortlicher',
            items: this.addNoneUser(this.valueAttributeOrDefault('users')),
            default: [],
            cast: (value) => map([value].flat(), (value) => { return value === null ? null : parseInt(value) })
          },
          {
            name: 'workflow_definition_ids',
            type: 'multi-select',
            label: 'Art der Maßnahme',
            items: this.valueAttributeOrDefault('workflow_definitions'),
            default: [],
            cast: (value) => map([value].flat(), parseInt)
          }],
        [
          {
            name: 'contributor_ids',
            type: 'user-multi-select',
            label: 'Teilnehmer',
            items: this.addNoneUser(this.valueAttributeOrDefault('users')),
            default: [],
            cast: (value) => map([value].flat(), (value) => { return value === null ? null : parseInt(value) })
          },
          {
            name: 'ambition_ids',
            type: 'multi-select',
            label: 'Ziel',
            items: this.valueAttributeOrDefault('ambitions'),
            default: [],
            cast: (value) => map([value].flat(), parseInt)
          }],
        [
          {
            name: 'workflow_assignee_ids',
            type: 'user-multi-select',
            label: 'Verantwortlicher der Maßnahme',
            items: this.addNoneUser(this.valueAttributeOrDefault('users')),
            default: [],
            cast: (value) => map([value].flat(), (value) => { return value === null ? null : parseInt(value) })
          }]
      ]
    }
  }
}
