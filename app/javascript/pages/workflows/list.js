import ListPage from '../list-page'
import ListContent from './list-content'
import map from 'lodash/map'
import parseInt from 'lodash/parseInt'

export default {
  name: 'WorkflowListPage',
  mixins: [ListPage],
  data () {
    return {
      filter: {
        page: 1,
        queryText: null,
        order: 'desc',
        tab: 'ACTIVE',
        assignees: [],
        contributors: [],
        workflowDefinitions: [],
        ambitions: []
      }
    }
  },
  computed: {
    pageTitleParts () {
      return ['Maßnahmen']
    },
    pageContentComponents () {
      return ListContent
    }
  },
  methods: {
    initPageRequestUrl () {
      return this.$apiEndpoints.workflows.index()
    },
    initPageRequestParams () {
      return {
        page: this.pages.current,
        query: this.filters.values.query,
        order: this.filters.values.order,
        tab_category: this.filters.values.tab_category,
        assignee_ids: this.filters.values.assignee_ids,
        contributor_ids: this.filters.values.contributor_ids,
        workflow_definition_ids: this.filters.values.workflow_definition_ids,
        ambition_ids: this.filters.values.ambition_ids
      }
    },
    onPropUpdated (prop, value, info) {
      ListPage.methods.onPropUpdated.call(this, prop, value, info)
      this.$router.replace({ name: 'workflows', query: this.createQuery() })
    },
    filterTabs () {
      return {
        name: 'tab_category',
        items: [
          { text: 'Aktiv', value: 'ACTIVE' },
          { text: 'Mit fälligen Aufgaben', value: 'WITH_DUE_TASKS' },
          { text: 'Alle', value: 'ALL' }
        ],
        counts: this.valueAttributeOrDefault('tab_categories', {}),
        default: 'ACTIVE'
      }
    },
    filterFields () {
      return [
        [
          {
            name: 'query',
            type: 'text',
            label: 'Maßnahmentitel',
            default: ''
          },
          {
            name: 'order',
            type: 'single-select',
            label: 'Sortierung',
            items: [
              { text: 'Erstellungsdatum – neueste zuerst', value: 'created_at_desc' },
              { text: 'Erstellungsdatum – älteste zuerst', value: 'created_at_asc' },
              { text: 'Titel – aufsteigend', value: 'title_asc' },
              { text: 'Titel – absteigend', value: 'title_desc' }
            ],
            default: 'created_at_desc',
            cols: 4
          }],
        [
          {
            name: 'assignee_ids',
            type: 'user-multi-select',
            label: 'Verantwortlicher',
            items: this.addNoneUser(this.valueAttributeOrDefault('users')),
            default: [],
            cast: (value) => map([value].flat(), (value) => { return value === null ? null : parseInt(value) })
          },
          {
            name: 'workflow_definition_ids',
            type: 'multi-select',
            label: 'Art der Maßnahme',
            items: this.valueAttributeOrDefault('workflow_definitions'),
            default: [],
            cast: (value) => map([value].flat(), parseInt)
          }],
        [
          {
            name: 'contributor_ids',
            type: 'user-multi-select',
            label: 'Teilnehmer',
            items: this.addNoneUser(this.valueAttributeOrDefault('users')),
            default: [],
            cast: (value) => map([value].flat(), (value) => { return value === null ? null : parseInt(value) })
          },
          {
            name: 'ambition_ids',
            type: 'multi-select',
            label: 'Ziel',
            items: this.valueAttributeOrDefault('ambitions'),
            default: [],
            cast: (value) => map([value].flat(), parseInt)
          }]
      ]
    }
  }
}
