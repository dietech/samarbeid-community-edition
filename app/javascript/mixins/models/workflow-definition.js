import { stateColors } from 'helpers/definitions'

export default {
  computed: {
    workflowDefinitionBreadcrumbs () {
      return [
        { text: 'Maßnahmevorlagen', to: { name: 'workflow-definitions' } },
        { text: `${this.workflowDefinition.id}` }
      ]
    },
    workflowDefinitionStateText () {
      return 'Aktualisiert'
    },
    workflowDefinitionStateColor () {
      return stateColors.created
    },
    workflowDefinitionStateUpdatedAtDate () {
      return new Date(this.workflowDefinition.updatedAt)
    }
  }
}
