export default {
  actions: {
    create: 'Maßnahmevorlage erstellen',
    delete: 'Maßnahmevorlage löschen'
  },
  createDialog: {
    title: 'Maßnahmevorlage erstellen',
    buttonOk: 'Maßnahmevorlage erstellen'
  },
  deleteDialog: {
    title: 'Maßnahmevorlage löschen?',
    text: 'Löschen Sie die Maßnahmevorlage, wenn Sie sicher sind, dass diese Maßnahmevorlage nicht mehr benöigt wird. Gelöschte Maßnahmevorlagen sind unwiederbringlich verloren.',
    buttonOk: 'Maßnahmevorlage löschen'
  }
}
