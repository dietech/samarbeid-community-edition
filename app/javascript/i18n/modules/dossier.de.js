export default {
  actions: {
    create: 'Dossier erstellen',
    export: 'Aktuelle Liste exportieren'
  },
  detailActions: {
    delete: 'Dossier löschen'
  },
  createDialog: {
    title: 'Dossier erstellen',
    buttonOk: 'Dossier erstellen',
    buttonBack: 'Zurück'
  },
  deleteDialog: {
    title: 'Dossier löschen?',
    text: 'Löschen Sie das Dossier, wenn Sie sicher sind, dass dieses Dossier nicht mehr benöigt wird.',
    buttonOk: 'Dossier löschen'
  }
}
