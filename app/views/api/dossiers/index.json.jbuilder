json.dossier_definitions @dossier_definitions.map { |pd| {value: pd.id, text: pd.name} }

json.total_pages total_page_count_for(@result)
json.result do
  json.partial! "/api/dossiers/dossier_list_item", collection: @result, as: :dossier
end
