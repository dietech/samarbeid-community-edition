json.array! @users do |user|
  json.partial! "/api/users/user", user: user
  json.mentionLabel user.mention_label
end
