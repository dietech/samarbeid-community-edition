if user.nil?
  json.null!
else
  json.id user.id
  json.fullname user.fullname
  json.email user.email
  json.deactivatedAt user.deactivated_at

  json.avatar do
    json.partial! "api/users/avatar", user: user, size: local_assigns[:avatar_size], show_deactivated: local_assigns[:show_deactivated]
  end
end
