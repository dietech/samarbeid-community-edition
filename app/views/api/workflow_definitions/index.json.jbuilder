json.total_pages total_page_count_for(@result)
json.result do
  json.partial! "/api/workflow_definitions/workflow_definition", collection: @result, as: :workflow_definition
end
