json.dataFields do
  json.partial! "/api/tasks/task_data_fields", data_fields: @task_items.map(&:as_datafield)
end
