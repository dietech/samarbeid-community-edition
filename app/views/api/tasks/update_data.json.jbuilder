json.workflow do
  json.partial! "/api/workflows/workflow", workflow: @workflow
end

json.dataFields do
  json.partial! "/api/tasks/task_data_fields", data_fields: @task_items.map(&:as_datafield)
end
