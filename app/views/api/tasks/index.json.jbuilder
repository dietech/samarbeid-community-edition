json.tab_categories Task.tab_categories_counts(@result)
  .map { |key, count| [key, key == "MARKED" ? @marked_count.total_count : count] }
  .to_h

json.users do
  json.partial! "/api/users/user", collection: @users, as: :user
end
json.workflow_definitions @workflow_definitions.map { |pd| {value: pd.id, text: pd.name} }
json.ambitions @ambitions.map { |pd| {value: pd.id, text: pd.title} }
json.task_definitions @task_definitions.map { |td| {value: td.id, text: td.name} }

json.total_pages total_page_count_for(@result)
json.result do
  json.partial! "/api/tasks/task", collection: @result, as: :task
end
