json.id @workflow.id
json.nextStep do
  next_step = Services::NextInterestingThing.new(@workflow, current_user).next_station
  json.type next_step.class.name.downcase
  json.id next_step.id
end
