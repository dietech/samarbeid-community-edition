json.tab_categories Workflow.tab_categories_counts(@result).to_h
json.users do
  json.partial! "/api/users/user", collection: @users, as: :user
end
json.workflow_definitions @workflow_definitions.map { |pd| {value: pd.id, text: pd.name} }
json.ambitions @ambitions.map { |pd| {value: pd.id, text: pd.title} }

json.total_pages total_page_count_for(@result)
json.result do
  json.partial! "/api/workflows/workflow_minimal", collection: @result, as: :workflow
end
