json.id @dossier_definition.id
json.gid @dossier_definition.to_global_id.to_s
json.name @dossier_definition.name
json.description @dossier_definition.description
json.updatedAt @dossier_definition.updated_at

json.groups do
  json.partial! "/api/groups/group", collection: @dossier_definition.groups.order(:name), as: :group
end

json.titleFields @dossier_definition.title_fields
json.subtitleFields @dossier_definition.subtitle_fields

json.fields @dossier_definition.fields do |field|
  json.id field.id
  json.name field.name
  json.position field.position
  json.type do
    json.name field.content_type.to_s
    json.label field.content_type.label
    json.partial! "/api/content_types/options", type: field.content_type, options: field.options
    json.options_definition field.content_type.options_definition
  end
  json.required field.required
  json.recommended field.recommended
  json.unique field.unique
end
