json.events do
  json.partial! "/api/events/event", collection: @events, as: :event
end

case @object
when Workflow
  json.workflow do
    json.partial! "/api/workflows/workflow", workflow: @object
  end
when Task
  json.workflow do
    json.partial! "/api/workflows/workflow", workflow: @object.workflow
  end
when Ambition
  json.ambition do
    json.partial! "/api/ambitions/ambition", ambition: @object
  end
end
