class Ambition < ApplicationRecord
  include SearchForIndexWithCategories
  include ReferenceSupport

  belongs_to :assignee, class_name: "User", optional: true

  has_many :contributions, as: :contributable, dependent: :destroy
  has_many :contributors, -> { order "contributions.created_at" }, through: :contributions, inverse_of: false, source: :user, after_add: :reindex_later, after_remove: :reindex_later

  has_and_belongs_to_many :workflows, class_name: "Workflow", association_foreign_key: :workflow_id,
                                      after_add: :reindex_workflows_and_self, after_remove: :reindex_workflows_and_self

  has_many :comments, as: :object, dependent: :destroy

  scope :not_deleted, -> { where(deleted: false) }

  validates :title, presence: true, length: {maximum: 80}

  searchkick language: "german", callbacks: :async, word_middle: [:identifier, :title, :subtitle], suggest: [:title, :subtitle]
  scope :search_import, -> { not_deleted }

  after_save :reindex_workflows_and_self

  def active_visible_workflows(user = nil)
    result = workflows.select(&:active?)
    result = result.select { |workflow| user.can?(:read, workflow) } if user
    result
  end

  def self.define_tab_categories
    [["OPEN", ->(a) { !a.closed }],
      ["SOME_WORKFLOWS_ACTIVE", ->(a) { !a.closed && a.active_visible_workflows.any? }],
      ["CLOSED", ->(a) { a.closed }],
      ["ALL", ->(_a) { true }]]
  end

  def self.defaults_for_search_for_index
    {order: {title: :asc}}
  end

  def search_data
    {
      identifier: [identifier, id.to_s],
      title: title&.downcase,
      subtitle: nil,
      description: description,
      created_at: created_at,
      comments: comments.map(&:message),
      tab_categories: tab_categories,
      assignee_id: assignee_id,
      contributor_ids: contributors.map(&:id),
      workflow_definition_ids: workflows.not_deleted.map(&:workflow_definition).uniq.map(&:id)
    }.merge(visibility_for_groups)
  end

  def visibility_for_groups
    {visible_for_groups: Group.all.map(&:id)}
  end

  def should_index?
    !deleted
  end

  def workflows=(new_workflows)
    removed_workflows = workflows - new_workflows
    super(new_workflows)
    reindex_workflows_and_self([removed_workflows + new_workflows].uniq)
  end

  def identifier
    "!#{id}"
  end

  # ReferenceSupport
  def reference_label(noaccess = false)
    identifier + (noaccess || deleted? ? "" : " • #{title}")
  end

  def mention_label(noaccess = false)
    reference_label(noaccess)
  end

  private

  def reindex_later(_params)
    reindex
  end

  def reindex_workflows_and_self(workflow = nil)
    if workflow.is_a?(Workflow) # called by habtm association
      workflow.reindex
    else
      workflows.find_each(&:reindex)
    end
    reindex
  end
end
