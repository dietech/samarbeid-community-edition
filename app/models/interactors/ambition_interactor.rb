class Interactors::AmbitionInteractor
  def self.create(user, attributes)
    ambition = Ambition.create(attributes)

    if ambition.valid?
      if ambition.update(assignee: user)
        add_contributor_if_needed(ambition, user)
        Events::CreatedEvent.create!(subject: user, object: ambition)
      end
    end

    ambition
  end

  def self.delete(ambition, user)
    unless ambition.deleted
      if ambition.update(deleted: true)
        Events::DeletedEvent.create!(subject: user, object: ambition)
      end
    end
  end

  def self.close(ambition, user)
    unless ambition.closed
      if ambition.update(closed: true, state_updated_at: Time.now)
        Events::CompletedEvent.create!(subject: user, object: ambition)
      end
    end
  end

  def self.reopen(ambition, user)
    if ambition.closed
      if ambition.update(closed: false, state_updated_at: ambition.created_at)
        Events::ReopenedEvent.create!(subject: user, object: ambition)
      end
    end
  end

  def self.update(ambition, params, user)
    old_title = ambition.title
    old_description = ambition.description

    ambition.title = params[:title] if params.key?(:title)
    if params.key?(:description) && params[:description].present?
      ambition.description, mentions = Services::Mentioning.extract_mentions(Services::EditorSanitizer.sanitize(params[:description]))
      user_mention_ids = Services::Mentioning.get_user_mentions(mentions)
    end

    if ambition.changed? && ambition.save
      if ambition.saved_change_to_title?
        Events::ChangedTitleEvent.create!(subject: user, object: ambition, data: {new_title: ambition.title, old_title: old_title})
      end

      if ambition.saved_change_to_description?
        old_user_mention_ids = Services::Mentioning.get_user_mentions(Services::Mentioning.get_mentions(old_description))
        user_mention_ids -= old_user_mention_ids
        mentioned_users = User.where(id: user_mention_ids)

        Events::ChangedSummaryEvent.create!(subject: user, object: ambition, data: {new_summary: ambition.description, old_summary: old_description, mentioned_users: mentioned_users})
        mentioned_users.each { |mu| add_contributor_if_needed(ambition, mu) }
      end

      add_contributor_if_needed(ambition, user)
    end
  end

  def self.update_assignee(ambition, assignee, user)
    old_assignee = ambition.assignee

    if old_assignee != assignee
      if ambition.update(assignee: assignee)
        if assignee.nil?
          Events::UnassignedEvent.create!(subject: user, object: ambition, data: {old_assignee: old_assignee})
        else
          Events::AssignedEvent.create!(subject: user, object: ambition, data: {new_assignee: assignee, old_assignee: old_assignee})
          add_contributor_if_needed(ambition, assignee)
        end

        add_contributor_if_needed(ambition, user)
      end
    end
  end

  def self.update_contributors(ambition, contributors, user)
    to_add = contributors - ambition.contributors
    to_remove = ambition.contributors - contributors

    if to_add.any? || to_remove.any?
      ambition.contributors = contributors

      if ambition.valid?
        to_add.each do |contributor|
          Events::AddedContributorEvent.create!(subject: user, object: ambition, data: {added_contributor: contributor})
        end

        to_remove.each do |contributor|
          Events::RemovedContributorEvent.create!(subject: user, object: ambition, data: {removed_contributor: contributor})
        end

        add_contributor_if_needed(ambition, user) unless to_remove.include?(user)
      end
    end
  end

  def self.update_workflows(ambition, workflows, user)
    to_add = workflows - ambition.workflows
    to_remove = ambition.workflows - workflows

    if to_add.any? || to_remove.any?
      ambition.workflows = workflows

      if ambition.valid?
        to_add.each do |workflow|
          Events::AddedWorkflowEvent.create!(subject: user, object: ambition, data: {added_workflow: workflow})
        end

        to_remove.each do |workflow|
          Events::RemovedWorkflowEvent.create!(subject: user, object: ambition, data: {removed_workflow: workflow})
        end

        add_contributor_if_needed(ambition, user)
      end
    end
  end

  def self.add_contributor_if_needed(ambition, contributor)
    ambition.contributors << contributor unless ambition.contributors.include?(contributor)
  end
  private_class_method :add_contributor_if_needed
end
