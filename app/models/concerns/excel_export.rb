require "active_support/concern"

module ExcelExport
  extend ActiveSupport::Concern

  included do
  end

  # Excel file with one page per Definition
  def to_excel(to_file = nil)
    package = Axlsx::Package.new

    @definitions_and_items.each do |definition, workflows|
      add_sheet_to_workbook(definition, workflows, package.workbook)
    end

    if to_file
      package.serialize(to_file)
    else
      string_io = package.to_stream
      string_io.string # Return plain string to use in send_data
    end
  end

  def add_sheet_to_workbook(definition, workflows, workbook)
    max_worksheet_name_length = definition.name.truncate(31)
    sheet = workbook.add_worksheet(name: max_worksheet_name_length)
    sheet.add_row base_columns + definition_columns(definition)

    workflows.each do |workflow|
      data_row = values_for_base_columns(workflow)
      data_row += values_for_definition_columns(workflow)
      excel_row = sheet.add_row
      data_row.each do |value|
        add_to_excel_row(value, excel_row)
      end
    end

    sheet.auto_filter = "A1:#{sheet.cells.last.r}"
  end

  def add_to_excel_row(thingy, row)
    row.add_cell(nil) and return unless thingy

    case thingy.class.name
    when Workflow.name
      cell = row.add_cell thingy.identifier
      row.worksheet.add_hyperlink location: frontend_link_for("workflow", thingy.id), ref: cell
    when Dossier.name
      cell = row.add_cell thingy.identifier
      row.worksheet.add_hyperlink location: frontend_link_for("dossier", thingy.id), ref: cell
    when ContentItem.name
      case thingy.content_type.name
      when ContentTypes::File.name
        cell = row.add_cell thingy.localized_value
        task = thingy.task_items&.first&.task
        row.worksheet.add_hyperlink location: frontend_link_for("task", task.id), ref: cell if task
      when ContentTypes::Url.name
        cell = row.add_cell thingy.localized_value
        row.worksheet.add_hyperlink location: thingy.localized_value, ref: cell
      else
        row.add_cell thingy.localized_value
      end
    when String.name
      row.add_cell thingy
    when Array.name
      if thingy.empty?
        row.add_cell nil
      else
        row.add_cell thingy.join(", ")
      end
    else
      row.add_cell thingy.inspect
    end
  end

  # Array of CSV Strings - one entry per WorkflowDefinition
  def to_csv_s
    @definitions_and_items.map do |workflow_definition, workflows|
      CSV.generate do |csv|
        csv << base_columns + definition_columns(workflow_definition)
        workflows.each do |workflow|
          row = values_for_base_columns(workflow) + values_for_definition_columns(workflow)
          csv_values = row.map { |thingy| value_for_table(thingy) }
          csv << csv_values
        end
      end
    end
  end

  def value_for_table(thingy)
    return nil unless thingy
    return frontend_link_for("workflow", thingy.id) if thingy.is_a?(Workflow)
    return frontend_link_for("dossier", thingy.id) if thingy.is_a?(Dossier)
    return thingy.localized_value if thingy.respond_to?(:localized_value)
    thingy.inspect
  end

  private

  def frontend_link_for(object_type, object_id)
    Object.new.extend(EmailHelper).object_reference_url({object_type: object_type, object_id: object_id})
  end
end
