class ContentTypes::File < ContentTypes::Value
  class << self
    def type
      :file
    end

    def cast(value)
      UploadedFile.where(id: value) unless value.nil?
    end

    def serialize(value)
      value&.map(&:to_global_id)&.map(&:to_s)
    end

    def deserialize(value)
      GlobalID::Locator.locate_many(value) unless value.nil?
    end

    def localized_string(value, options)
      value.map(&:title).join(", ")
    end

    def data_empty?(value)
      super || value.length == 0
    end

    def options_schema
      {
        type: "object",
        properties: {
          multiple: {
            type: "boolean"
          }
        },
        additionalProperties: false
      }
    end
  end
end
