class ContentTypes::Boolean < ContentTypes::Value
  class << self
    def type
      :boolean
    end

    def cast(value)
      ActiveModel::Type::Boolean.new.cast(value) unless value.nil?
    end

    def localized_string(value, options)
      value === true ? "Ja" : "Nein"
    end

    def validate_data(value)
      if value.to_s.in?(["true", "false"])
        true
      else
        "muss 'true' oder 'false' sein"
      end
    end
  end
end
