class ContentTypes::Email < ContentTypes::String
  class << self
    def type
      :email
    end

    def cast(value)
      super(value&.strip)
    end

    def validate_data(value)
      if value.empty? || /\A((.*)<)?([^@<\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})(?(1)>|)\z/i.match?(value)
        true
      else
        "muss eine E-Mail-Adresse sein"
      end
    end
  end
end
