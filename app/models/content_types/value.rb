class ContentTypes::Value
  class << self
    def type
      raise "Not implemented"
    end

    def to_s
      type.to_s
    end

    def label
      I18n.t "content_type.#{type}.label"
    end

    def deserialize(value)
      cast(value) unless value.nil?
    end

    def cast(value)
      value unless value.nil?
    end

    def serialize(value)
      value&.as_json
    end

    def localized_string(value, options)
      value.to_s
    end

    def data_empty?(value)
      value.nil?
    end

    def validate_data(value)
      true
    end

    def options_schema
      nil
    end

    def validate_options(options)
      return true unless options_schema

      result = JSON::Validator.fully_validate(options_schema, options)
      result.empty? ? true : result
    end

    def options_definition
      return nil unless options_schema

      result = options_schema.except(:additionalProperties)
      reorganize_options_properties!(result, "content_type.#{type}.options")

      result
    end

    private

    def reorganize_options_properties!(options_def, i18n_key)
      options_def.except!(:type)

      required_options = options_def[:required] || []
      options_def.except!(:required)

      if options_def.has_key?(:properties)
        options_def[:properties].each do |property_type, property_def|
          reorganize_options_property!(property_def, required_options.include?(property_type.to_s), "#{i18n_key}.#{property_type}")
        end

        properties = options_def[:properties]
        options_def.except!(:properties)
        options_def.merge!(properties)
      end
    end

    def reorganize_options_property!(property_def, required, i18n_key)
      if property_def.has_key?(:format)
        property_def[:type] = property_def[:format]
        property_def.except!(:format)
      end
      property_def.except!(:enum)
      property_def[:required] = required

      property_def[:title] = I18n.t("#{i18n_key}.label") if I18n.exists?("#{i18n_key}.label")

      if property_def[:type] === "array" && property_def.has_key?(:items)
        reorganize_options_properties!(property_def[:items], "#{i18n_key}.items")
      end
    end
  end
end
