class ContentTypes::Text < ContentTypes::String
  class << self
    def type
      :text
    end

    def localized_string(value, options)
      value&.squish
    end
  end
end
