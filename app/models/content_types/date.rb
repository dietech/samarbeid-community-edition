class ContentTypes::Date < ContentTypes::Value
  class << self
    def type
      :date
    end

    def cast(value)
      ActiveModel::Type::Date.new.cast(value) unless value.nil?
    end

    def localized_string(value, options)
      I18n.l(value)
    end
  end
end
