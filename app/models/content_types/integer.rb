class ContentTypes::Integer < ContentTypes::Value
  class << self
    def type
      :integer
    end

    def cast(value)
      ActiveModel::Type::Integer.new.cast(value) unless value.nil?
    end

    def localized_string(value, options)
      ActiveSupport::NumberHelper.number_to_delimited(value)
    end
  end
end
