class Services::DossiersToTable
  include ExcelExport

  def initialize(dossiers = [])
    @definitions_and_items = dossiers.group_by(&:definition).sort_by { |d, _| d.name }
  end

  def base_columns
    ["Dossier-ID", "Dossiertitel", "Erstellungsdatum"]
  end

  def definition_columns(dossier_definition)
    field_definitions_sorted(dossier_definition).map(&:name)
  end

  def values_for_base_columns(dossier)
    [dossier, dossier.title, dossier.created_at.strftime("%Y-%m-%d %H:%M")]
  end

  def values_for_definition_columns(workflow)
    values_for_content_item_definitions_in(workflow)
  end

  def values_for_content_item_definitions_in(dossier)
    field_definitions_sorted(dossier.definition).map do |field|
      ContentItem.new(label: field.name,
        content_type: field.content_type.to_s.to_sym, options: field.options,
        value: dossier.get_field_value(field.id))
    end
  end

  def field_definitions_sorted(definition)
    definition.fields.sort_by(&:position)
  end
end
