class Events::RemovedWorkflowEvent < Event
  validate :validate_event_type_values

  validates :subject, presence: true

  def removed_workflow
    data&.[](:removed_workflow)
  end

  alias_method :old_value, :removed_workflow

  def notification_receivers
    [object.assignee].compact - [subject]
  end

  private

  def validate_event_type_values
    validate_object_is_one_of(Ambition)
    errors.add(:data, "value for key :removed_workflow should be a workflow") unless removed_workflow.is_a?(Workflow)
  end
end
