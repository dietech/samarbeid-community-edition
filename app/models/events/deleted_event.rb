class Events::DeletedEvent < Event
  validate :validate_event_type_values

  validates :subject, presence: true

  def notification_receivers
    receivers = (object.contributors | [object.assignee])
    receivers |= [object.workflow.assignee] if object.is_a?(Task)
    receivers |= object.all_tasks.map(&:assignee) if object.is_a?(Workflow)

    receivers.compact - [subject]
  end

  private

  def validate_event_type_values
    validate_object_is_one_of(Ambition, Workflow, Task)
  end
end
