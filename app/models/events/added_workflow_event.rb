class Events::AddedWorkflowEvent < Event
  validate :validate_event_type_values

  validates :subject, presence: true

  def added_workflow
    data&.[](:added_workflow)
  end

  alias_method :new_value, :added_workflow

  def notification_receivers
    [object.assignee].compact - [subject]
  end

  private

  def validate_event_type_values
    validate_object_is_one_of(Ambition)
    errors.add(:data, "value for key :added_workflow should be a workflow") unless added_workflow.is_a?(Workflow)
  end
end
