class Events::StartedEvent < Event
  validate :validate_event_type_values

  validates :subject, presence: true, unless: -> { object.is_a?(Task) }

  def notification_receivers
    [object.assignee].compact - [subject]
  end

  private

  def validate_event_type_values
    validate_object_is_one_of(Workflow, Task)
  end
end
