class Events::ChangedTitleEvent < Event
  validate :validate_event_type_values

  validates :subject, presence: true

  def new_title
    data&.[](:new_title)
  end

  def old_title
    data&.[](:old_title)
  end

  alias_method :new_value, :new_title
  alias_method :old_value, :old_title

  def notification_receivers
    return User.admin.all - [subject] if object.is_a?(WorkflowDefinition) || object.is_a?(DossierDefinition)
    return [object.assignee] - [subject] if object.assignee.present?
    return [object.workflow.assignee] - [subject] if object.is_a?(Task) && object.workflow.assignee.present?
    []
  end

  private

  def validate_event_type_values
    validate_object_is_one_of(Ambition, Workflow, Task, WorkflowDefinition, DossierDefinition)
    errors.add(:data, "value for key :new_title should be a string") unless new_title.is_a?(String)
    errors.add(:data, "value for key :old_title should be a string") unless old_title.nil? || old_title.is_a?(String)
  end
end
