class ContentItemDefinition < ApplicationRecord
  include ContentTypeSupport

  belongs_to :workflow_definition
  has_many :task_item_definitions, dependent: :destroy
  has_many :content_items, dependent: :destroy
  has_one :block_definition, dependent: :nullify # TODO: has_many :block_definitions

  scope :not_deleted, -> { where(deleted: false) }

  validates :label, uniqueness: {scope: :workflow_definition_id}
  validates :label, presence: true
  validates :label, format: {without: /\./, message: "darf keine Punkte enthalten"}
  validate :validate_default_value

  def default_value
    content_type.deserialize(read_attribute(:default_value))
  end

  def default_value=(new_value)
    casted_new_value = content_type.serialize(content_type.cast(new_value))
    return if casted_new_value == default_value

    write_attribute(:default_value, casted_new_value)
  end

  def rails_admin_label
    "#{workflow_definition&.rails_admin_label} > #{id}-#{label&.parameterize}"
  end

  def build_content_item(new_workflow)
    ContentItem.new(label: label, content_type: content_type.to_s, options: options, value: default_value,
      workflow: new_workflow, content_item_definition: self)
  end

  private

  def validate_default_value
    return if default_value.nil?
    validation_result = content_type.validate_data(default_value)
    errors.add(:default_value, validation_result) if validation_result != true
  end
end
