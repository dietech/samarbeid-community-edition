class ContentType
  class << self
    def types_array
      # The order of this array determines the order of selections in the frontend
      [
        ContentTypes::String,
        ContentTypes::Text,
        ContentTypes::Richtext,
        ContentTypes::Url,
        ContentTypes::Email,
        ContentTypes::Dossier,
        ContentTypes::File,
        ContentTypes::Selection,
        ContentTypes::Boolean,
        ContentTypes::Date,
        ContentTypes::Time,
        ContentTypes::Datetime,
        ContentTypes::Integer,
        ContentTypes::Decimal
      ]
    end

    def types_hash
      types_array.map { |ct| [ct.type.to_sym, ct] }.to_h
    end

    def enum_values
      types_array.map { |ct| [ct.type.to_s, ct.type.to_s] }.to_h

      # Using the label created problems when reading content_type and even content_type_before_type_cast returned
      # the label instead of the type
      # types_array.map { |ct| [ct.label, ct.type.to_s] }.to_h
    end
  end
end
