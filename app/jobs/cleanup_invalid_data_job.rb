class CleanupInvalidDataJob < ApplicationJob
  queue_as :default

  def perform
    UploadedFile.destroy_invalid
    Event.destroy_invalid
  end
end
