class DeliverRecentNotificationsMailsJob < ApplicationJob
  queue_as :default

  def perform(*_args)
    User.for_sending_notifications.each do |current_user|
      notifications = current_user.unseen_and_unsent_notifications
      next if notifications.empty?

      NotificationsMailer.with(user: current_user, notifications: notifications).recent.deliver_now

      notifications.where(delivered_at: nil).update_all(delivered_at: Time.zone.now)
      current_user.persist_sending_notification_and_schedule_next
    end
  end
end
