class CleanupNotificationsJob < ApplicationJob
  queue_as :default

  def perform
    Notification.cleanup
  end
end
