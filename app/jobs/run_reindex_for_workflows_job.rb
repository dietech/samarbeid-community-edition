class RunReindexForWorkflowsJob < ApplicationJob
  queue_as :default

  def perform(workflow_definition)
    workflow_definition.workflows.reindex
  end
end
