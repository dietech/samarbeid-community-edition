# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).

puts "SEEDing Database a demo user admin@example.org, Hello-World Group, Workflow and Task"

CustomElasticSearchConfig.initalize_searchkick_indexes

admin = User.create!(email: "admin@example.org", password: "password", firstname: "Change", lastname: "Me", role: :super_admin)
g_hello_world = Group.create(name: "Hallo-Welt-Gruppe")
admin.groups << g_hello_world

dd_planet = DossierDefinition.create!(
  name: "Planet",
  groups: [g_hello_world]
)

dd_planet_dfd_name = DossierFieldDefinition.create!(
  definition: dd_planet,
  name: "Name",
  position: 10,
  content_type: "string"
)

dd_planet.title_fields = [dd_planet_dfd_name.id]
dd_planet.save!

Dossier.create!(
  definition: dd_planet,
  data: {dd_planet_dfd_name.id.to_s.to_sym => "Erde"}
)

wd = WorkflowDefinition.create!(
  name: "Hallo Welt",
  description: "So einfach kann eine Maßnahme in samarbeid sein. Einfach starten und loslegen.",
  groups: [g_hello_world]
)

td = TaskDefinition.create!(
  name: "Los geht's",
  description: "Zuerst werden alle begrüßt.",
  position: 10,
  workflow_definition: wd,
  definition_workflow_or_block: wd
)

cid1 = ContentItemDefinition.create!(
  label: "Was ich der Welt schon immer sagen wollte",
  content_type: "text",
  workflow_definition: wd
)

TaskItemDefinition.create!(
  task_definition: td,
  content_item_definition: cid1,
  position: 10,
  workflow_definition: wd
)

cid2 = ContentItemDefinition.create!(
  label: "Planet, dem ich Hallo sage",
  content_type: "dossier",
  options: {type: dd_planet.id, multiple: true},
  workflow_definition: wd
)

TaskItemDefinition.create!(
  task_definition: td,
  content_item_definition: cid2,
  position: 20,
  workflow_definition: wd
)

ambition = Interactors::AmbitionInteractor.create(
  admin, {
    title: "Jedem Planeten Hallo sagen",
    description: "Dieses Ziel bitte löschen, wenn die Arbeit mit samarbeid richtig losgeht."
  }
)
Interactors::WorkflowInteractor.create(wd, {title: "HAWE-1", assignee_id: admin.id}, ambition, nil, admin)
