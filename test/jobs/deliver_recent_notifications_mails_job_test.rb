require "test_helper"

class DeliverRecentNotificationsMailsJobTest < ActiveJob::TestCase
  class EmailDeliveryObserver
    attr_accessor :delivered_emails
    def initialize
      self.delivered_emails = []
    end

    def count
      delivered_emails.length
    end

    def delivered_email(message)
      delivered_emails << message
    end
  end

  test "running job should work" do
    assert_equal 0, User.for_sending_notifications.length

    user = User.first

    # trigger a notification
    workflow = Workflow.first
    Interactors::WorkflowInteractor.update_assignee(workflow, user, User.second)

    assert_equal 1, User.for_sending_notifications.length

    job_email_delivery_observer = EmailDeliveryObserver.new
    ActionMailer::Base.register_observer(job_email_delivery_observer)
    assert_nothing_raised { DeliverRecentNotificationsMailsJob.perform_now }
    ActionMailer::Base.unregister_observer(job_email_delivery_observer)

    user.reload

    assert_equal 0, User.for_sending_notifications.length
    assert_equal 1, job_email_delivery_observer.count

    mail = job_email_delivery_observer.delivered_emails.first
    assert_equal "admin@example.org", mail.to.first
    assert_equal "no-reply@example.org", mail.from.first
    assert_match(/\Asamarbeid \| .*hat Nutzer Admin User als Verantwortlichen von Maßnahme .* zugewiesen/, mail.subject)
    assert_equal 0, mail.errors.length

    assert mail.multipart?

    text = mail.text_part.decoded
    assert_match(/Hallo Admin User/, text)
    assert_match(/Du hast 1 neue Benachrichtigung in samarbeid/, text)
    assert_match(%r{https://samarbeid-tests.example.com/workflows/7}, text)
    assert_match(%r{https://samarbeid-tests.example.com/notifications}, text)
    assert_match(%r{https://samarbeid-tests.example.com/my/settings/notifications}, text)

    html = Capybara.string mail.html_part.decoded
    html.assert_selector 'a[href="https://samarbeid-tests.example.com/users/1"]', text: "Admin User"
    html.assert_selector 'a[href="https://samarbeid-tests.example.com/workflows/7"]', text: "%7 • EB-7"

    event = workflow.events.last
    html.assert_selector "a[href=\"https://samarbeid-tests.example.com/workflows/7?event=#{event.id}#timeline-item-assigned-#{event.id}\"]", text: "Öffnen"
    html.assert_selector 'a[href="https://samarbeid-tests.example.com/my/settings/notifications"]', text: "Einstellungen"
  end

  test "deactivated users shouldn't receive notification emails" do
    assert_equal 0, User.for_sending_notifications.length

    current_user = User.find_by!(email: "team-admin@example.org")
    user = User.find_by!(email: "srs_user@example.org")

    # trigger a notification
    workflow = Workflow.find_by!(aasm_state: :active, assignee: user)
    Interactors::WorkflowInteractor.update_assignee(workflow, nil, current_user)
    assert_equal 1, User.for_sending_notifications.length

    user.deactivate
    assert_equal 0, User.for_sending_notifications.length
  end
end
