require "test_helper"

class ContributionTest < ActiveSupport::TestCase
  def setup
    @user = User.first
  end

  test "can belong to either a workflow, a task or an ambition" do
    workflow = Workflow.first
    contribution_workflow = Contribution.create!(contributable: workflow, user: @user)

    assert_equal(contribution_workflow.contributable, workflow)
  end

  test "user has many contributions and contributed_workflows" do
    workflow1 = Workflow.first
    workflow2 = Workflow.second

    assert_difference -> { @user.contributions.count } => 2, -> { @user.contributed_workflows.count } => 2 do
      Contribution.create!(contributable: workflow1, user: @user)
      Contribution.create!(contributable: workflow2, user: @user)
    end

    assert_equal workflow1, @user.contributed_workflows.second_to_last
    assert_equal workflow2, @user.contributed_workflows.last
  end

  test "workflow has many contributions and contributors with correct order" do
    workflow = Workflow.first
    user1 = @user
    user2 = User.second

    assert_difference -> { workflow.contributions.count } => 2, -> { workflow.contributors.count } => 2 do
      Contribution.create!(contributable: workflow, user: user1)
      Contribution.create!(contributable: workflow, user: user2)
    end

    assert_equal user1, workflow.contributors.second_to_last
    assert_equal user2, workflow.contributors.last
  end
end
