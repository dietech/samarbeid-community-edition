require "test_helper"

class TaskSearchTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  setup do
    @user = User.find_by!(email: "admin@example.org")
  end

  test "#tab_categories should report correctly" do
    workflow = WorkflowDefinition.find_by!(name: "Erstgespräch Erfindung").create_workflow!
    task = workflow.items.first
    assert_equal ["TODO", "MARKED", "ALL"], task.reload.tab_categories
    task.update(due_at: 3.days.ago)
    assert_equal ["TODO", "DUE", "MARKED", "ALL"], task.reload.tab_categories
    task.assignee = @user
    task.start!
    assert_equal ["TODO", "DUE", "MARKED", "ALL"], task.reload.tab_categories
    task.complete!
    assert_equal ["MARKED", "ALL"], task.reload.tab_categories
    another_task = workflow.items.last
    another_task.update(assignee: @user) && another_task.enable! && another_task.start!
    another_task.skip!
    assert_equal ["MARKED", "ALL"], another_task.reload.tab_categories
  end

  test "#tab_categories_counts should report correctly" do
    CustomElasticSearchConfig.reindex_and_refresh(Task)

    results = Task.search_for_index(@user, "*", true)
    assert_equal [["TODO", 15], ["DUE", 2], ["MARKED", 104], ["ALL", 104]],
      Task.tab_categories_counts(results)
  end

  test "tasks should be found by their identifier and parts of title / subtitle" do
    CustomElasticSearchConfig.reindex_and_refresh(Task)

    task = Task.find(47) # Beschreibung erstellen - Patentanmeldung > PA-1
    queries = ["#47", "47"]
    queries.each do |query|
      assert_equal task, Task.search_for_list(@user, query).first, "Search by Task.search_for_list for '#{query}' did not find correct result"
    end
    assert Task.search_for_list(@user, "Patentanmeldung PA-1").include?(task)
  end

  test "deleted task or those with deleted or trashed workflows should not be indexed" do
    task = Task.first
    assert Task.search_import.include?(task)
    assert task.should_index?

    task.update!(aasm_state: "deleted")

    refute Task.search_import.include?(task)
    refute task.should_index?

    task.update!(aasm_state: "active")
    task.workflow.update!(aasm_state: "deleted")

    refute Task.search_import.include?(task)
    refute task.should_index?

    task.workflow.update!(aasm_state: "trashed")

    refute Task.search_import.include?(task)
    refute task.should_index?
  end

  test "due_at_for_sorting should show most due tasks (oldest) first and then created_at newest first" do
    very_due_task, due_task, normal_task, very_old_task = Task.all.limit(4)
    very_due_task.update!(due_at: 1.year.ago)
    due_task.update!(due_at: 1.week.ago)
    normal_task.update!(due_at: nil, created_at: Time.now)
    very_old_task.update!(due_at: nil, created_at: 1.year.ago)

    CustomElasticSearchConfig.reindex_and_refresh(Task)

    tasks_by_due = Task.search("*", order: {due_at_for_sorting: :asc}).to_a
    assert tasks_by_due.index(very_due_task) < tasks_by_due.index(due_task)
    assert tasks_by_due.index(due_task) < tasks_by_due.index(normal_task)
    assert tasks_by_due.index(normal_task) < tasks_by_due.index(very_old_task)
  end
end
