require "test_helper"

class NotificationLifecycleSupportTest < ActiveSupport::TestCase
  def setup
    @user = User.first
    @event = Events::DueEvent.create!(object: Task.first)
    @notification = Notification.create(user: @user, event: @event)
  end

  test "is initially undelivered, unread, unbookmarked and undone" do
    refute @notification.delivered?
    refute @notification.read?
    refute @notification.bookmarked?
    refute @notification.done?
  end

  test "is delivered" do
    @notification.delivered!
    assert @notification.delivered?
    refute @notification.read?
    refute @notification.bookmarked?
    refute @notification.done?
  end

  test "is read (and automatically delivered)" do
    @notification.read!
    assert @notification.delivered?
    assert @notification.read?
    refute @notification.bookmarked?
    refute @notification.done?
  end

  test "is bookmarked (and automatically delivered and read)" do
    @notification.bookmark!
    assert @notification.delivered?
    assert @notification.read?
    assert @notification.bookmarked?
    refute @notification.done?
  end

  test "is done (and automatically delivered and read)" do
    @notification.done!
    assert @notification.delivered?
    assert @notification.read?
    refute @notification.bookmarked?
    assert @notification.done?
  end

  test "is done (and automatically unbookmarked and vice versa)" do
    @notification.bookmark!
    assert @notification.bookmarked?
    refute @notification.done?
    @notification.done!
    refute @notification.bookmarked?
    assert @notification.done?
    @notification.bookmark!
    assert @notification.bookmarked?
    refute @notification.done?
  end

  test "is undone" do
    @notification.done!
    assert @notification.delivered?
    assert @notification.read?
    refute @notification.bookmarked?
    assert @notification.done?

    @notification.undone!
    assert @notification.delivered?
    assert @notification.read?
    refute @notification.bookmarked?
    refute @notification.done?
  end

  test "is undone (artificial case, just for information)" do
    @notification.undone!
    refute @notification.delivered?
    refute @notification.read?
    refute @notification.bookmarked?
    refute @notification.done?
  end

  test "is unbookmarked (fresh)" do
    @notification.bookmark!

    @notification.unbookmark!
    assert @notification.delivered?
    assert @notification.read?
    refute @notification.bookmarked?
    refute @notification.done?
  end

  test "is unbookmarked (obsolete)" do
    @notification.bookmark!
    @new_event = Events::DueEvent.create!(object: Task.first)

    assert @event.reload.obsolete?

    @notification.unbookmark!
    assert @notification.delivered?
    assert @notification.read?
    refute @notification.bookmarked?
    assert @notification.done? # because the event is obsolete
  end

  test "is unbookmarked (old read)" do
    @notification.update(read_at: 2.weeks.ago - 1.minute)
    @notification.bookmark!

    refute @event.reload.obsolete?

    @notification.unbookmark!
    assert @notification.delivered?
    assert @notification.read?
    refute @notification.bookmarked?
    assert @notification.done? # because the notification was read more than 2 weeks ago
  end
end
