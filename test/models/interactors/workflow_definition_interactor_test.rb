require "test_helper"

class Interactors::WorkflowDefinitionInteractorTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  def setup
    @user = User.find_by!(email: "admin@example.org")
    @workflow_definition = WorkflowDefinition.find_by!(name: "Bürgeranfrage")
  end

  test "creating definitions should also create an event" do
    assert_difference -> { WorkflowDefinition.count } do
      assert_difference -> { Events::CreatedEvent.count } do
        Interactors::WorkflowDefinitionInteractor.create({name: "A new name"}, @user)
      end
    end
  end

  test "updating definitions should work" do
    new_groups = Group.pluck(:id)
    Interactors::WorkflowDefinitionInteractor.update(@workflow_definition, {name: "A new name", description: "Something different",
                      group_ids: new_groups}, @user)
    @workflow_definition.reload

    assert_equal "A new name", @workflow_definition.name
    assert_equal "Something different", @workflow_definition.description
    assert_equal Group.all.sort, @workflow_definition.groups.sort
  end

  test "destroying definitions should work only if no instances exist" do
    workflow_definition = WorkflowDefinition.create!(name: "Empty")
    assert_difference -> { WorkflowDefinition.count }, -1 do
      Interactors::WorkflowDefinitionInteractor.destroy(workflow_definition, @user)
    end

    @workflow_definition.create_workflow!
    assert_no_difference -> { WorkflowDefinition.count } do
      Interactors::WorkflowDefinitionInteractor.destroy(@workflow_definition, @user)
    end
    refute_empty @workflow_definition.errors
  end

  test "updating structure should create apropriate event" do
    workflow = @workflow_definition.create_workflow!
    assert_difference -> { Events::ChangedStructureEvent.for_object(@workflow_definition).count } do
      Interactors::WorkflowDefinitionInteractor.update_structure(workflow, @user)
    end
    changed_structure_event = Events::ChangedStructureEvent.for_object(@workflow_definition).order(:created_at).last
    assert_equal @workflow_definition.version, changed_structure_event.new_workflow_definition_version
    assert_equal workflow, changed_structure_event.workflow_used_for_update
  end
end
