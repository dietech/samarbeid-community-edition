require "test_helper"

class Interactors::WorkflowInteractorTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  def setup
    @user = User.find_by!(email: "admin@example.org")
    @other_user = User.find_by!(email: "srs_user@example.org")
    @workflow_definition = WorkflowDefinition.first

    @running_workflow = @workflow_definition.create_workflow!
  end

  test "#create should create workflow instance with assignee, contributor and ambition, create events and reindex all tasks" do
    ambition = Ambition.where(closed: false).first
    workflow = nil

    assert_difference -> { Events::CreatedEvent.count }, @workflow_definition.all_task_definitions.count do
      assert_difference -> { Events::StartedEvent.for_object(workflow).count } do
        assert_difference -> { Workflow.count } do
          assert_enqueued_with(job: Searchkick::ReindexV2Job) do
            workflow = Interactors::WorkflowInteractor.create(
              @workflow_definition, {title: "Test-Workflow", assignee_id: @user.id}, ambition, nil, @user
            )
          end
        end
      end
    end

    assert_equal @user, workflow.assignee
    assert workflow.contributors.include?(@user)
    assert workflow.ambitions.include?(ambition)
  end

  test "#create with predecessor_workflow should create new workflow and set predecessor_workflow and create events" do
    predecessor_workflow = Workflow.first
    workflow = nil

    assert_difference -> { Events::CreatedEvent.count }, @workflow_definition.all_task_definitions.count do
      assert_difference -> { Events::StartedEvent.for_object(workflow).count } do
        assert_difference -> { Events::AddedSuccessorWorkflowEvent.for_object(predecessor_workflow).count } do
          workflow = Interactors::WorkflowInteractor.create(
            @workflow_definition, {title: "Test-Workflow"}, nil, predecessor_workflow, @user
          )
        end
      end
    end

    assert_equal predecessor_workflow, workflow.predecessor_workflow
    assert predecessor_workflow.successor_workflows.include?(workflow)
  end

  test "#trash should trash workflow, add contributor if necessary, create an event and reindex all tasks" do
    assert_enqueued_with(job: Searchkick::ReindexV2Job) do
      Interactors::WorkflowInteractor.trash(@running_workflow, @user)
    end

    assert @running_workflow.trashed?
    assert @running_workflow.contributors.include?(@user)
  end

  test "#reopen should reopen workflow, add contributor if necessary, create an event and reindex all tasks" do
    completed_workflow = @running_workflow.tap(&:complete!)

    assert_enqueued_with(job: Searchkick::ReindexV2Job) do
      Interactors::WorkflowInteractor.reopen(completed_workflow, @user)
    end

    assert completed_workflow.active?
    assert completed_workflow.contributors.include?(@user)
  end

  test "#complete should work for any active workflow" do
    assert_difference -> { Events::CompletedEvent.for_object(@running_workflow).count } do
      assert_enqueued_with(job: Searchkick::ReindexV2Job) do
        Interactors::WorkflowInteractor.complete(@running_workflow, @user)
      end
    end
    assert @running_workflow.completed?
    assert @running_workflow.contributors.include?(@user)
  end

  test "#update_assignee with a user value should set the assignee of the workflow, add contributor if necessary, create an event and reindex all tasks" do
    # assign
    assert_difference -> { Events::AssignedEvent.for_object(@running_workflow).count } do
      assert_enqueued_with(job: Searchkick::ReindexV2Job) do
        Interactors::WorkflowInteractor.update_assignee(@running_workflow, @user, @user)
      end
    end

    assert_equal @user, @running_workflow.assignee
    assert @running_workflow.contributors.include?(@user)
  end

  test "#update_assignee with a nil value should remove the assignee of the workflow, add contributor if necessary, create an event and reindex all tasks" do
    @running_workflow.update!(assignee: @user)

    assert_difference -> { Events::UnassignedEvent.for_object(@running_workflow).count } do
      assert_enqueued_with(job: Searchkick::ReindexV2Job) do
        Interactors::WorkflowInteractor.update_assignee(@running_workflow, nil, @user)
      end
    end

    assert_nil @running_workflow.assignee
    assert @running_workflow.contributors.include?(@user)
  end

  test "#update_contributors should add and remove users as contributors of the workflow and if necessary, create an event and reindex all tasks" do
    removed_contributor = User.find_by!(email: "srs_user@example.org")
    @running_workflow.contributors << removed_contributor

    assert_difference -> { Events::AddedContributorEvent.for_object(@running_workflow).count } do
      assert_difference -> { Events::RemovedContributorEvent.for_object(@running_workflow).count } do
        assert_enqueued_with(job: Searchkick::ReindexV2Job) do
          Interactors::WorkflowInteractor.update_contributors(@running_workflow, User.where(id: @user.id), @user)
        end
      end
    end

    assert @running_workflow.contributors.include?(@user)
    assert_not @running_workflow.contributors.include?(removed_contributor)
  end

  test "#update_contributors should remove current_user from the contributors of the workflow and create an event" do
    @running_workflow.contributors << @user
    assert @running_workflow.contributors.include?(@user)

    assert_difference -> { Events::RemovedContributorEvent.for_object(@running_workflow).count } do
      Interactors::WorkflowInteractor.update_contributors(@running_workflow, @running_workflow.contributors - [@user], @user)
    end

    assert_not @running_workflow.contributors.include?(@user)
  end

  test "#update should change the title of the workflow, add contributor if necessary, create an event and reindex all tasks" do
    update_params = {title: "test title"}

    assert_difference -> { Events::ChangedTitleEvent.for_object(@running_workflow).count } do
      assert_enqueued_with(job: Searchkick::ReindexV2Job) do
        Interactors::WorkflowInteractor.update(@running_workflow, update_params, @user)
      end
    end

    assert_equal update_params[:title], @running_workflow.title
    assert @running_workflow.contributors.include?(@user)
  end

  test "#update the description of the workflow with a mentioning of a user should work" do
    other_user = User.find_by!(email: "srs_user@example.org")

    params = ActionController::Parameters.new({
      workflow: {
        description: "<p>Changed Description <mention m-id=\"#{other_user.id}\" m-type=\"user\"></mention></p>"
      }
    })

    assert_difference -> { Events::ChangedSummaryEvent.for_object(@running_workflow).count } do
      Interactors::WorkflowInteractor.update(@running_workflow, params.require(:workflow).permit(:description), @user)
    end

    assert_equal params[:workflow][:description], @running_workflow.description
    assert @running_workflow.contributors.include?(@user)
    assert Events::ChangedSummaryEvent.for_object(@running_workflow).last.mentioned_users.include?(other_user)
  end
end
