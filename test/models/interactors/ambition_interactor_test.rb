require "test_helper"

class Interactors::AmbitionInteractorTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  def setup
    @user = User.find_by!(email: "admin@example.org")
    @ambition = Ambition.create(title: "Neues Test-Ziel")
  end

  test "#create should create ambition instance with assignee and contributor and create an event" do
    ambition = nil

    assert_difference -> { Events::CreatedEvent.count } do
      assert_difference -> { Ambition.count } do
        ambition = Interactors::AmbitionInteractor.create(@user, {title: "test ambition"})
      end
    end

    assert_equal @user, ambition.assignee
    assert ambition.contributors.include?(@user)
  end

  test "#delete should set ambition as deleted and create event" do
    assert_difference -> { Ambition.not_deleted.count }, -1 do
      assert_difference -> { Events::DeletedEvent.for_object(@ambition).count } do
        Interactors::AmbitionInteractor.delete(@ambition, @user)
      end
    end
  end

  test "#close should close ambition and create an event" do
    assert_difference -> { Events::CompletedEvent.for_object(@ambition).count } do
      Interactors::AmbitionInteractor.close(@ambition, @user)
    end

    assert @ambition.closed
  end

  test "#reopen should reopen ambition and create an event" do
    @ambition.update!(closed: true)

    assert_difference -> { Events::ReopenedEvent.for_object(@ambition).count } do
      Interactors::AmbitionInteractor.reopen(@ambition, @user)
    end

    assert_not @ambition.closed
  end

  test "#update should update the title and/or description of the ambition, add the current_user as contributor and create an event" do
    other_user = User.find_by!(email: "srs_user@example.org")

    params = ActionController::Parameters.new({
      ambition: {
        title: "Changed Title",
        description: "<p>Changed Description <mention m-id=\"#{other_user.id}\" m-type=\"user\"></mention></p>"
      }
    })

    assert_difference -> { Events::ChangedTitleEvent.for_object(@ambition).count } do
      assert_difference -> { Events::ChangedSummaryEvent.for_object(@ambition).count } do
        Interactors::AmbitionInteractor.update(@ambition, params.require(:ambition).permit(:title, :description), @user)
      end
    end

    assert_equal params[:ambition][:title], @ambition.title
    assert_equal params[:ambition][:description], @ambition.description
    assert @ambition.contributors.include?(@user)
    assert Events::ChangedSummaryEvent.for_object(@ambition).last.mentioned_users.include?(other_user)
  end

  test "#update_assignee should set an new assignee of the ambition, add the user as contributor and create an event" do
    assert_difference -> { Events::AssignedEvent.for_object(@ambition).count } do
      Interactors::AmbitionInteractor.update_assignee(@ambition, @user, @user)
    end

    assert_equal @user, @ambition.assignee
    assert @ambition.contributors.include?(@user)
  end

  test "#update_assignee should clear the assignee of the ambition, add the user as contributor and create an event" do
    @ambition.update!(assignee: @user)

    assert_difference -> { Events::UnassignedEvent.for_object(@ambition).count } do
      Interactors::AmbitionInteractor.update_assignee(@ambition, nil, @user)
    end

    assert_nil @ambition.assignee
    assert @ambition.contributors.include?(@user)
  end

  test "#update_contributors should add an user as contributor of the ambition, add the current user as contributor and create an event" do
    other_user = User.find_by!(email: "srs_user@example.org")

    assert_difference -> { Events::AddedContributorEvent.for_object(@ambition).count } do
      Interactors::AmbitionInteractor.update_contributors(@ambition, @ambition.contributors + [other_user], @user)
    end

    assert @ambition.contributors.include?(other_user)
    assert @ambition.contributors.include?(@user)
  end

  test "#update_contributors should remove an user from the contributors of the ambition, add the current user as contributor and create an event" do
    other_user = User.find_by!(email: "srs_user@example.org")
    @ambition.contributors << other_user

    assert @ambition.contributors.include?(other_user)

    assert_difference -> { Events::RemovedContributorEvent.for_object(@ambition).count } do
      Interactors::AmbitionInteractor.update_contributors(@ambition, @ambition.contributors - [other_user], @user)
    end

    assert_not @ambition.contributors.include?(other_user)
    assert @ambition.contributors.include?(@user)
  end

  test "#update_contributors should remove current_user from the contributors of the ambition and create an event" do
    @ambition.contributors << @user

    assert @ambition.contributors.include?(@user)

    assert_difference -> { Events::RemovedContributorEvent.for_object(@ambition).count } do
      Interactors::AmbitionInteractor.update_contributors(@ambition, @ambition.contributors - [@user], @user)
    end

    assert_not @ambition.contributors.include?(@user)
  end

  test "#update_workflows should add a workflow to the ambition, add the current user as contributor and create an event" do
    workflow = Workflow.first

    assert_difference -> { Events::AddedWorkflowEvent.for_object(@ambition).count } do
      Interactors::AmbitionInteractor.update_workflows(@ambition, @ambition.workflows + [workflow], @user)
    end

    assert @ambition.workflows.include?(workflow)
    assert @ambition.contributors.include?(@user)
  end

  test "#update_workflows should remove a workflow from the ambition, add the current user as contributor and create an event" do
    workflow = Workflow.first
    @ambition.workflows << workflow

    assert @ambition.workflows.include?(workflow)

    assert_difference -> { Events::RemovedWorkflowEvent.for_object(@ambition).count } do
      Interactors::AmbitionInteractor.update_workflows(@ambition, @ambition.workflows - [workflow], @user)
    end

    assert_not @ambition.workflows.include?(workflow)
    assert @ambition.contributors.include?(@user)
  end
end
