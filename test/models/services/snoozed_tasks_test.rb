require "test_helper"

class Services::SnoozedTasksTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  test "find_unprocessed should only find those not yet processed" do
    task = Task.first
    Timecop.freeze do
      task.update!(start_at: 1.hour.ago)
      assert Task.with_start_at_in_past.include?(task)

      task.update!(start_at: Time.now)
      assert Task.with_start_at_in_past.include?(task)

      task.update!(start_at: 1.second.from_now)
      refute Task.with_start_at_in_past.include?(task)

      task.update!(start_at: nil)
      refute Task.with_start_at_in_past.include?(task)
    end
  end

  test "find_and_start should do everything necessary" do
    task = WorkflowDefinition.first.create_workflow!.direct_tasks.last
    task.update(start_at: 1.hour.ago)

    reindex_job_for_task = {job: Searchkick::ReindexV2Job, args: [task.class.name, task.id.to_s, nil, {routing: nil}]}

    assert_enqueued_with(**reindex_job_for_task) do
      assert_difference -> { Events::StartedEvent.for_object(task).count } do
        Services::SnoozedTasks.find_and_start
      end
    end
    assert task.reload.start_at.nil?
  end
end
