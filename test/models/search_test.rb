require "test_helper"

class SearchTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  def setup
    CustomElasticSearchConfig.initalize_searchkick_indexes
  end

  test "simple Workflow.search should work" do
    ambition = Ambition.first
    workflow = WorkflowDefinition.find_by!(name: "Patentanmeldung").create_workflow!
    workflow.ambitions << ambition

    Workflow.searchkick_index.refresh # This triggers ES to update index immediately and not after 1 second as otherwise

    assert workflow, Workflow.search(ambition.title).first
  end

  test "Fulltext Search should work" do
    my_search = Services::Search.new

    assert_not_empty my_search.fulltext
  end

  test "titles should be weighted higher than other attributes" do
    ambition = Ambition.find(2) # Blockchain basierte Nutella
    workflow = Workflow.first
    perform_enqueued_jobs { Workflow.first.ambitions << ambition }

    CustomElasticSearchConfig.sync_all_indexes

    results = Services::Search.new.fulltext("Nutella", {per_page: 1000}).to_a

    assert results.include?(workflow) # because it's Ambition is Nutella
    assert_equal ambition, results.first
  end

  test "workflows should not be found if not in group" do
    private_workflow = Workflow.find(17) # Geburtstagsparty
    srs_user = private_workflow.assignee
    assert Services::Search.new(srs_user).fulltext("Geburtstag").include?(private_workflow)

    no_access_user = User.find_by(email: "no_access_user@example.org")
    refute Services::Search.new(no_access_user).fulltext("Geburtstag").include?(private_workflow)
  end

  test "tasks, workflows, ambitions should be found via their comments" do
    query = "suchTestKommentare"
    results = Services::Search.new.fulltext(query)
    assert results.include?(Task.find(59))
    assert results.include?(Workflow.find(13))
    assert results.include?(Ambition.find(2))
  end

  test "tasks, workflows, ambitions should be found by their identifier" do
    assert Services::Search.new.fulltext("#59").first(5).include?(Task.find(59))
    assert Services::Search.new.fulltext("%13").first(5).include?(Workflow.find(13))
    assert Services::Search.new.fulltext("!2").first(5).include?(Ambition.find(2))
  end

  test "search should work with umlaute and other special chars" do
    a_ambition = Ambition.first

    perform_enqueued_jobs { a_ambition.update!(description: "Dies ist ein Text mit Umläutendenerer und anschließendß mit 😉.") }
    CustomElasticSearchConfig.sync_all_indexes

    assert_equal a_ambition, Services::Search.new.fulltext("Umläutendenerer").first
    assert_equal a_ambition, Services::Search.new.fulltext("anschließendß").first
    assert_equal a_ambition, Services::Search.new.fulltext("😉").first
  end

  test "search should suggest better queries" do
    misspelled_query = "Freiher" # is stemmed to "freih" and thus even with misspellings search does not find "Freiherr"

    suggestions = Dossier.search(misspelled_query, suggest: true).suggestions
    assert_equal "freiherr", suggestions.first

    fulltext_suggestions = Services::Search.new.fulltext(misspelled_query).suggestions
    assert_equal "freiherr", fulltext_suggestions.first
  end

  test "search should find workflows which reference dossiers" do
    person_referenced_in_workflows = Dossier.find(21) # Gräfin Tristan von der Bormann
    results = Services::Search.new.fulltext("Gräfin Tristan von der Bormann")
    assert_equal person_referenced_in_workflows, results.first
    assert results.include?(Workflow.find(7))
    assert results.include?(Workflow.find(16))
  end

  test "indexing strings with leading/trailing dots and other Elasticsearch limitations should not cause exceptions" do
    assert_nothing_raised do
      perform_enqueued_jobs { Ambition.first.update!(title: ".leading-dot") }
    end

    dossier_definition = DossierDefinition.find_by(name: "Person")
    dossier_field_definition = dossier_definition.fields.find_by(name: "E-Mail")
    # dossier = dossier_definition.dossiers.first
    # dossier.set_field_value("email", "must-be-present-to-trigger-indexing@example.org")
    # dossier.save!

    # pp dossier.send(:search_data)

    assert_nothing_raised do
      perform_enqueued_jobs { dossier_field_definition.update(name: ".leading-dot") }
      Dossier.reindex
    end

    content_item = ContentItem.where(content_type: "string").first
    content_item.update!(value: "must be present to trigger indexing")

    assert_nothing_raised do
      perform_enqueued_jobs { content_item.update(label: "") }
    end

    assert_nothing_raised do
      perform_enqueued_jobs { content_item.update(label: ".leading-dot") }
    end

    assert_nothing_raised do
      perform_enqueued_jobs { content_item.update(label: "a trailing-dot.") }
    end

    assert_nothing_raised do
      perform_enqueued_jobs { content_item.update(label: "foo..bar.baz") }
    end
  end
end
