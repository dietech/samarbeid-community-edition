require "test_helper"

class WorkflowSearchTest < ActiveSupport::TestCase
  setup do
    @user = User.find_by_email!("admin@example.org")
    @workflow = Workflow.first
  end

  test "#search_for_index should work" do
    CustomElasticSearchConfig.reindex_and_refresh(Workflow)

    results = Workflow.search_for_index(@user)
    assert_not_empty results
  end

  test "#tab_categories should report correctly" do
    workflow = Workflow.first
    assert_equal ["ACTIVE", "WITH_DUE_TASKS", "ALL"], workflow.tab_categories
    assert_equal ["ACTIVE", "WITH_DUE_TASKS", "ALL"], workflow.tab_categories
    workflow.all_tasks.each { |t| t.update(aasm_state: "completed") }
    assert_equal ["ACTIVE", "ALL"], workflow.reload.tab_categories
    workflow.complete!
    assert_equal ["ALL"], workflow.reload.tab_categories
  end

  test "#tab_categories_counts should report correctly" do
    CustomElasticSearchConfig.reindex_and_refresh(Workflow)

    results = Workflow.search_for_index(@user, "*", true)
    assert_equal [["ACTIVE", 14], ["WITH_DUE_TASKS", 2], ["ALL", 15]],
      Workflow.tab_categories_counts(results)
  end

  test "#search_for_index with aggregates and additional_query_params should work" do
    CustomElasticSearchConfig.reindex_and_refresh(Workflow)

    additional_params = {
      where: {
        tab_categories: "ALL"
      },
      order: {created_at: :desc}
    }
    result = Workflow.search_for_index(@user, @workflow.title, true, additional_params)

    assert_equal [@workflow], result.to_a
  end

  test "workflows should be found by their identifier and parts of title" do
    CustomElasticSearchConfig.reindex_and_refresh(Workflow)

    workflow = Workflow.find(13) # srs-worfklow
    queries = ["%13", "13", "srs"]
    queries.each do |query|
      assert_equal workflow, Workflow.search_for_list(@user, query).first, "Search by Workflow.search_for_list for '#{query}' did not find correct result"
    end
    assert Workflow.search_for_list(@user, "Patentanmeldung").include?(workflow)
  end

  test "deleted or trashed workflows should not be indexed" do
    workflow = Workflow.first
    assert Workflow.search_import.include?(workflow)
    assert workflow.should_index?

    workflow.update!(aasm_state: "trashed")

    refute Workflow.search_import.include?(workflow)
    refute workflow.should_index?

    workflow.update!(aasm_state: "deleted")

    refute Workflow.search_import.include?(workflow)
    refute workflow.should_index?
  end
end
