require "test_helper"

class Api::DossierFieldDefinitionsControllerTest < ActionDispatch::IntegrationTest
  def setup
    CustomElasticSearchConfig.reindex_and_refresh(DossierDefinition)

    @dossier_definition = DossierDefinition.find_by!(name: "DOSSIER FÜR AUTOMATISIERTE TESTS")
    @dossier_field_definition = @dossier_definition.fields.second

    @team_admin = User.find_by!(email: "team-admin@example.org")
    login_as @team_admin
  end

  test "#create should create new dossier field definition" do
    assert_difference -> { DossierFieldDefinition.count } do
      post api_dossier_field_definitions_path, as: :json, params: {
        dossier_definition_id: @dossier_definition.id,
        name: "New Field",
        content_type: ContentTypes::String.to_s,
        options: {}
      }
    end
    assert_response :success
  end

  test "#update should change a dossier field definition" do
    patch api_dossier_field_definition_path(@dossier_field_definition), as: :json, params: {
      name: "Updated Field"
    }
    assert_response :success
  end

  test "#update shouldn't change the content_type of a dossier field definition if not compatible" do
    integer_field = @dossier_definition.fields.find_by!(content_type: ContentTypes::Integer.to_s)

    patch api_dossier_field_definition_path(integer_field), as: :json, params: {
      content_type: ContentTypes::String.to_s
    }
    assert_response :unprocessable_entity
  end

  test "#destroy should delete a dossier field definition" do
    assert_difference -> { DossierFieldDefinition.count }, -1 do
      delete api_dossier_field_definition_path(@dossier_field_definition, format: :json)
    end
    assert_response :success
  end

  test "#move should reorder the fields of a dossier definition by the attribute position" do
    refute_equal @dossier_definition.fields.last, @dossier_field_definition

    patch move_api_dossier_field_definition_path(@dossier_field_definition), as: :json, params: {
      index: @dossier_definition.fields.count
    }

    assert_response :success
    assert_equal @dossier_definition.fields.last, @dossier_field_definition
  end
end
