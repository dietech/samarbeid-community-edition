require "test_helper"

class Api::DossierDefinitionsControllerTest < ActionDispatch::IntegrationTest
  def setup
    CustomElasticSearchConfig.reindex_and_refresh(DossierDefinition)

    @dossier_definition = DossierDefinition.find_by!(name: "DOSSIER FÜR AUTOMATISIERTE TESTS")

    @team_admin = User.find_by!(email: "team-admin@example.org")
    login_as @team_admin
  end

  test "should get list" do
    logout
    login_as User.find_by!(email: "srs_user@example.org")

    get list_api_dossier_definitions_path(format: :json)
    assert_response :success
  end

  test "should get index" do
    get api_dossier_definitions_path(format: :json)
    assert_response :success
  end

  test "should get show" do
    get api_dossier_definition_path(@dossier_definition, format: :json)
    assert_response :success
  end

  test "should create new dossier definition" do
    assert_difference -> { DossierDefinition.count } do
      post api_dossier_definitions_path, as: :json, params: {
        name: "A new name",
        description: "A new description",
        group_ids: Group.all.map(&:id)
      }
    end
    assert_response :success
  end

  test "should update" do
    patch api_dossier_definition_path(@dossier_definition), as: :json, params: {
      name: "A new name",
      description: "Something different",
      group_ids: Group.pluck(:id),
      title_fields: @dossier_definition.title_fields |= [@dossier_definition.fields.last.id],
      subtitle_fields: @dossier_definition.subtitle_fields - [@dossier_definition.subtitle_fields.last]
    }
    assert_response :success
  end

  test "should destroy dossier definition without instances" do
    dossier_definition = DossierDefinition.create!(name: "Empty", groups: @team_admin.groups)
    assert_difference -> { DossierDefinition.count }, -1 do
      delete api_dossier_definition_path(dossier_definition, format: :json)
    end
    assert_response :success
  end

  test "shouldn't destroy dossier definition with any instances" do
    assert_not_empty @dossier_definition.dossiers

    assert_no_difference -> { DossierDefinition.count } do
      delete api_dossier_definition_path(@dossier_definition, format: :json)
    end
    assert_response :unprocessable_entity
  end
end
