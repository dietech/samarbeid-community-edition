require "test_helper"

class ApiControllerTest < ActionDispatch::IntegrationTest
  test "convert_null_to_nil should work as expected" do
    api_controller = ApiController.new
    params = ["null", 2, 4, "other", nil]
    assert_equal [nil, 2, 4, "other", nil], api_controller.send(:convert_null_to_nil, params)
  end
end
