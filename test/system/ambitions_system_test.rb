require "application_system_test_case"
require "system_test_helper"

class AmbitionSystemTest < ApplicationSystemTestCase
  setup do
    @user = User.find_by!(email: "srs_user@example.org")
    login_as @user
    @ambition = Ambition.first
  end

  test "creating and deleting an ambition should work" do
    visit "/ambitions"

    click_button "Ziel erstellen"
    create_ambition_dialog = page.find(".create-ambition-dialog", match: :first)
    within(create_ambition_dialog) do
      fill_in "Titel des neuen Ziels", with: "Test Ziel"
      click_card_action_button "ZIEL ERSTELLEN"
    end

    assert_css ".ambition-edit-page"
    assert_css ".v-breadcrumbs .v-breadcrumbs__item", text: "Ziele"
    assert_css ".page-detail-header .v-chip__content", text: "Eröffnet"

    open_vertical_dots_context_menu
    click_menu_list_item "Ziel löschen"
    click_dialog_button "LÖSCHEN"

    assert_current_path "/ambitions"
  end

  test "updating title and description and canceling edit of an ambition should work" do
    ambition_title = "ambition test title"
    ambition_description = "ambition test description"

    visit "/ambitions"
    click_on @ambition.title

    open_title_and_description_card
    fill_in_title_and_description ambition_title, ambition_description
    click_title_and_description_edit_button "SPEICHERN"

    assert_text ambition_title
    assert_text ambition_description

    open_title_and_description_card
    fill_in_title_and_description "#{ambition_title}_changed", "#{ambition_description}_changed"
    click_title_and_description_edit_button "ABBRECHEN"

    assert_text ambition_title
    assert_text ambition_description
  end

  test "adding/removing an existing workflow to/from an ambition should work" do
    @ambition.workflows.delete_all
    workflow = Workflow.active.first

    visit "/ambitions"
    click_on @ambition.title

    assert page.has_no_css?(".custom-object-control .v-card .v-list .v-list-item--link", text: workflow.title)

    click_ambition_workflow_select_button
    page.find(".custom-object-control .v-card .v-menu__content .v-list .v-list-item--link", text: workflow.title).click
    click_ambition_workflow_select_button

    assert page.has_css?(".custom-object-control .v-card .v-list .v-list-item--link", text: workflow.title)

    click_ambition_workflow_select_button
    page.find(".custom-object-control .v-card .v-menu__content .v-list .v-list-item--link", text: workflow.title).click
    click_ambition_workflow_select_button

    assert page.has_no_css?(".custom-object-control .v-card .v-list .v-list-item--link", text: workflow.title)
  end

  test "closing and reopening an ambition should work, closed ambitions should not allow to add/remove workflows" do
    visit "/ambitions"
    click_on @ambition.title

    open_vertical_dots_context_menu

    click_menu_list_item "Ziel abschließen"
    click_dialog_button "ZIEL ABSCHLIESSEN"

    # closed ambition should not allow to add or remove a workflow
    assert page.find(".custom-object-control .v-card .v-card__title div", text: "Zu erledigende Maßnahmen")
      .find(:xpath, "..")
      .has_css?("button:disabled", text: "ÄNDERN")

    open_vertical_dots_context_menu
    click_menu_list_item "Ziel wiedereröffnen"

    open_vertical_dots_context_menu
    assert has_menu_list_item("Ziel abschließen")
  end
end
