require "application_system_test_case"
require "system_test_helper"

class EventSystemTest < ApplicationSystemTestCase
  setup do
    @user = User.find_by!(email: "admin@example.org")
    login_as @user
  end

  test "ActivityHub should show events/history for all possible object types" do
    ambition = Ambition.find_by!(title: "event-test-ambition")
    visit "/ambitions/#{ambition.id}"

    page.find(".activity-hub .v-tabs .v-tab", text: "ALLE").click
    assert page.has_css?(".activity-hub-item-comment", count: Event.for_object(ambition).count { |e| e.is_a?(Events::CommentedEvent) })
    assert page.has_css?(".activity-hub-item-event", count: Event.for_object(ambition).count { |e| !e.is_a?(Events::CommentedEvent) })

    workflow = Workflow.find_by!(title: "event-test-workflow")
    visit "/workflows/#{workflow.id}"

    page.find(".activity-hub .v-tabs .v-tab", text: "ALLE").click
    assert page.has_css?(".activity-hub-item-comment", count: Event.for_object(workflow).count { |e| e.is_a?(Events::CommentedEvent) })
    assert page.has_css?(".activity-hub-item-event", count: Event.for_object(workflow).count { |e| !e.is_a?(Events::CommentedEvent) })

    task = Workflow.find_by!(title: "event-test-workflow").tasks_ordered_in_structure.first
    visit "/tasks/#{task.id}"

    page.find(".activity-hub .v-tabs .v-tab", text: "ALLE").click
    assert page.has_css?(".activity-hub-item-comment", count: Event.for_object(task).count { |e| e.is_a?(Events::CommentedEvent) })
    assert page.has_css?(".activity-hub-item-event", count: Event.for_object(task).count { |e| !e.is_a?(Events::CommentedEvent) })
  end

  test "updating and commenting of a workflows should work and create a notification for the mentioned user" do
    login_as(User.find_by!(email: "srs_user@example.org"))
    visit "/workflows"

    click_on "Bestimmung Externer Bedarfe"

    # Edit title and description
    new_title = "New workflow test title"
    new_description = "New workflow test description"
    open_title_and_description_card
    fill_in_title_and_description new_title, new_description
    click_title_and_description_edit_button "SPEICHERN"
    assert_text new_title
    assert_text new_description

    # Comment and mention a user.
    mention_user = User.find_by!(email: "admin@example.org")
    mention_user.update!(groups: Group.all) # ensure he may really access workflow TODO: Add second fixture User for SRS / Tomsy Team
    new_comment = "Ein kleiner Kommentar. Benachrichtigen möchte ich "

    activity_hub_element = page.find(".activity-hub")
    editor = find_prosemirror_editor(activity_hub_element)

    # Can only mention users with access to workflow/task
    prosemirror_fill_in(editor, new_comment + "@no_access_user")
    assert_css(".mention-suggestions-menu .v-list-item", text: "Keine Ergebnisse")

    prosemirror_fill_in(editor, new_comment + "@admin")
    page.find(".mention-suggestions-menu .v-list-item", text: mention_user.email).click
    activity_hub_element.click_button("Kommentieren")
    activity_hub_element.find(".v-timeline-item").assert_text new_comment + mention_user.mention_label

    # Mentioned user should have received his notification
    logout_using_menu
    login_as(mention_user)
    visit root_url
    page.find("#user-notifications-button").click
    item = page.find(".v-timeline-item.activity-hub-item-comment", text: new_comment + mention_user.mention_label)
    item.assert_text "SRS User hat Dich in einem Kommentar zu Maßnahme %7 • New workflow test title erwähnt • vor weniger als einer Minute"
  end
end
