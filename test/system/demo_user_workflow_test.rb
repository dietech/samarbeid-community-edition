require "application_system_test_case"
require "system_test_helper"

class DemoUserWorkflowTest < ApplicationSystemTestCase
  # This test is what during a typical short demonstration a user would do. See more for background in #686
  # If not startet with PERFORMANCE_MEASUREMENT=truish it behaves like a normal system test.
  # If startet as PERFORMANCE_MEASUREMENT the user waits random times (between 0.1 and 0.6 seconds) between actions
  #
  # To run this as an actual performance test you should startup as many browsers as parallel users are expected.
  # Prepare:
  # Use admin_system_test run against app to create users. Don't forget to check they actually have access to Workflow and Dossier used below.
  # Do not load fixtures or Index Elastic -> test_helper.rb comment out "fixtures :all", comment out calls to Reindex ES etc.
  # Use setup as described in admin_system_test (real host etc.)
  # Create a "Demo einer Veranstaltung" Workflow
  # Run (in zsh) put as many numbers in for as users:
  # $ for x (1 2 3 4 5); do WORKER_ID=$x PERFORMANCE_MEASUREMENT=true PARALLEL_WORKERS=1 TEST=test/system/demo_user_workflow_test.rb rake test & done
  test "do whatever a demo user is supposed to do" do
    maybe_setup_for_performance_test

    visit root_path
    user_looks_around # Look around for 0.1 - 0.6 seconds if running a PERFORMANCE_MEASUREMENT

    login_using_form_with("test-nutzer-#{ENV["WORKER_ID"]}@example.org")
    user_looks_around

    # Nach dem Login klicke Ich ein wenig herum und warte immer bis die Seite geladen ist

    header_area.click_on "Ziele"
    assert_text "ZIEL ERSTELLEN"
    user_looks_around
    header_area.click_on "Maßnahmen"
    assert_text "MASSNAHME STARTEN"
    user_looks_around
    header_area.click_on "Aufgaben"
    assert_text "FÄLLIG" # Aufgaben
    user_looks_around
    header_area.click_on "Dossiers"
    assert_text "DOSSIER ERSTELLEN"
    user_looks_around
    fill_in "fulltext-search", with: "Test\n"
    assert_text "Suche"
    user_looks_around
    header_area.find(".mdi-bell").click
    assert_text "Benachrichtigungen"
    user_looks_around
    header_area.find(".mdi-account").click
    assert_text "FÄLLIG" # Aufgaben
    user_looks_around
    header_area.find(".site-header-home-link").click
    assert_text "Benachrichtigungen"
    user_looks_around

    # Jetzt geht es los.
    # Dann werden sie auf die Liste der Maßnahmen klicken.
    within(header_area) { click_on "Maßnahmen" }
    user_looks_around

    # Da drin wird es eine Demo-Maßnahme geben. Die wird angeklickt.
    within(middle_area) do
      fill_in "Maßnahmentitel", with: "Demo einer Veranstaltung"
      user_looks_around
      click_on "Demo einer Veranstaltungsplanung"
      # Vielleicht wird jeder mal einnen kurzen Kommentar in der Maßnahme schreiben.

      user_looks_around
      comment_area = find_prosemirror_editor(page.find(".activity-hub"))
      3.times do |i|
        prosemirror_fill_in(comment_area, "@test-nutzer-#{i}")
        user_looks_around
        page.find(".mention-suggestions-menu .v-list-item", match: :first).click
      end
      prosemirror_fill_in(comment_area, " Ich probiere auch einen Kommentar zu schreiben. Liebe Grüße von Test User #{ENV["WORKER_ID"]}")
      click_button("Kommentieren")
      user_looks_around
    end

    # In der Maßnahme werden nach und nach alle Aufgaben einmal angeklickt.
    left_sidebar.all("a.v-list-item").each do |workflow_or_task|
      workflow_or_task.click
      user_looks_around
    end

    # Danach auf die Liste der Dossiers gegangen.
    # Und ein Dossier angeklickt.
    within(header_area) { click_on "Dossiers" }
    within(middle_area) do
      assert_text("DOSSIER ERSTELLEN")
      user_looks_around
      all(".v-list-item").first.click
      user_looks_around
    end

    # Jetzt machen wir noch ein paar weitere Dinge für die es bisher keine Tests gab...

    # Und erstellt sich dann selbst als Dossier
    within(header_area) { click_on "Dossiers" }
    within(middle_area) do
      assert_text("DOSSIER ERSTELLEN")
      user_looks_around
      click_on "Dossier erstellen"
      user_looks_around
    end

    create_dossier_dialog = page.find(".create-dossier-dialog", match: :first)
    within(create_dossier_dialog) do
      fill_in "Suche", with: "Pers"
      click_list_item_link "Person"
      assert_text "erforderlich" # Erforderliche Felder
      user_looks_around
      fill_in "E-Mail", with: "test-nutzer-#{ENV["WORKER_ID"]}@example.org"
      fill_in "Name", with: "Nachname#{ENV["WORKER_ID"]}"
      user_looks_around
      click_card_action_button "DOSSIER ERSTELLEN"
      user_looks_around
    end

    # Dann erstellt sich der User eine neue Maßnahme für seinen Geburtstag
    within(header_area) { click_on "Maßnahmen" }
    within(middle_area) do
      user_looks_around
      click_on "Maßnahme starten"
      user_looks_around
    end

    create_workflow_dialog = page.find(".create-workflow-dialog", match: :first)
    within(create_workflow_dialog) do
      fill_in("Suche", with: "Veranstaltung")
      user_looks_around
      click_list_item_link("Veranstaltungsplanung")
      fill_in("Titel der Maßnahme", with: "Test-Maßnahme")
      click_card_action_button("MASSNAHME STARTEN")
      user_looks_around
    end

    assert_css ".task-edit-page"
    within middle_area do
      assert has_text?("Art der Veranstaltung angeben") # we are redirected to next task
    end

    within(middle_area) do
      user_looks_around
      click_on "Aufgabe starten"
      user_looks_around
      fill_in "Working title", with: "Tolle Party!"
      user_looks_around
      click_on "Aufgabe abschließen"
      assert has_text?("Veranstalter angeben") # wait for next task to load
    end
    logout_using_menu
  end

  private

  def user_looks_around
    return unless ENV["PERFORMANCE_MEASUREMENT"]
    sleep(0.1 + rand(0.5))
  end

  def maybe_setup_for_performance_test
    if ENV["PERFORMANCE_MEASUREMENT"] # we should be doing a performance test against a real system
      # give other processes enough headroom to start browser instances
      sleep 10
      visit root_path
      puts "Worker ID: #{ENV["WORKER_ID"]}"
      sleep 10
    else
      # setup for normal system tests
      10.times do |i|
        u = User.create(email: "test-nutzer-#{i}@example.org", password: "password", firstname: "No", lastname: "Group")
        u.groups = Group.all
      end
      Interactors::WorkflowInteractor.create(
        WorkflowDefinition.find_by(name: "Veranstaltungsplanung"), {title: "Demo einer Veranstaltungsplanung"}, nil, nil, User.find_by!(email: "admin@example.org")
      )
      ENV["WORKER_ID"] = "0"
      CustomElasticSearchConfig.initalize_searchkick_indexes
    end
  end
end
