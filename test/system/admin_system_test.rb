require "application_system_test_case"
require "system_test_helper"

class AdminSystemTest < ApplicationSystemTestCase
  def setup
    logout
  end

  # This test may be used to create any number of users for performance testing
  # To allow this one must change the Capybara default host in application_system_test_case as such:
  #     Capybara.app_host = "https://user:pwd@alpha-canary.samarbeid.org" # maybe you need some kind of auth
  #     Capybara.run_server = false # don't start Rack
  # and change the number of users to create (loop)
  # then simply start "test" run with:
  # $ SYSTEM_TEST_BROWSER=chrome PARALLEL_WORKERS=1 TEST=test/system/admin_system_test.rb rake test
  test "manage users and groups and workflow definitions" do
    visit root_path
    login_using_form_with("team-admin@example.org")

    visit root_path
    click_button("admin-menu")
    click_link("Gruppen")
    click_on "Gruppe erstellen"

    create_group_dialog = page.find(".create-group-dialog", match: :first)
    within(create_group_dialog) do
      fill_in("Name", with: "Gruppe-für-Performance-Test")
      click_card_action_button("GRUPPE ERSTELLEN")
    end

    CustomElasticSearchConfig.reindex_and_refresh(Group)

    n = 2
    n.times do |i|
      visit root_path
      click_button("admin-menu")
      click_link("Nutzer")
      click_on "Nutzer erstellen"

      create_user_dialog = page.find(".create-user-dialog", match: :first)
      within(create_user_dialog) do
        fill_in("E-Mail", with: "test-nutzer-#{i}@example.org")
        fill_in("Vorname", with: "Test #{i}")
        fill_in("Nachname", with: "User")
        fill_in("Passwort", with: "password")
        fill_in("Passwort wiederholen", with: "password")
        click_card_action_button("NUTZER ERSTELLEN")
      end

      assert_text "Aktualisiert" # is shown on user page

      visit root_path
      click_button("admin-menu")
      click_link("Gruppen")
      click_on "Gruppe-für-Performance-Test"

      click_button("Ändern", match: :first)
      fill_in("Suche", with: "test-nutzer-#{i}@example.org")
      find(".v-list-item", text: "test-nutzer-#{i}@example.org").click
      click_button("Ändern", match: :first)
      assert_text "Erfolgreich gespeichert"
    end

    visit root_path
    click_button("admin-menu")
    click_link("Maßnahmevorlagen")

    click_on "Neu erstellte Vorlage - zum ausprobieren" # not visible to team-admin but shown here
    assert_text "DISKUSSION" # We can see the page did not find suitable css-class to look for
  end
end
