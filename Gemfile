source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby File.read(".ruby-version").strip.delete_prefix("ruby-")

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem "rails", "~> 6.1.0"
# Use PostgreSQL as the database for Active Record
gem "pg"
# Use Puma as the app server
gem "puma"
# Use SCSS for stylesheets
gem "sassc-rails"
gem "sprockets", "<4"
# Use Uglifier as compressor for JavaScript assets
gem "uglifier", ">= 1.3.0"

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem "jbuilder", "~> 2.5"

# Use ActiveStorage variant
gem "image_processing", "~> 1.2"

gem "active_storage_validations"

# Reduces boot times through caching; required in config/boot.rb
gem "bootsnap", ">= 1.1.0", require: false

# Use Webpack to manage app-like JavaScript modules in Rails
gem "webpacker", "~> 5.x"

# Rails engine that provides an easy-to-use interface for managing your data
gem "rails_admin"
# Simple Has Many (Collection) Widget for RailsAdmin that is much simpler looking than the default has_many association field
gem "rails_admin_simple_has_many", github: "Dietech-Group/rails_admin_simple_has_many"

# Sentry Exception Tracking
gem "sentry-ruby"
gem "sentry-rails"

# Intelligent search made easy with Rails and Elasticsearch
gem "searchkick"

# AASM - State machines for Ruby classes (plain Ruby, ActiveRecord, Mongoid, NoBrainer)
gem "aasm"

# Multithreaded, Postgres-based, ActiveJob backend for Ruby on Rails.
gem "good_job"

# Flexible authentication solution for Rails with Warden.
gem "devise"
# Translations for the devise gem
gem "devise-i18n"
# The authorization Gem for Ruby on Rails.
gem "cancancan"

# Nokogiri (鋸) is a Rubygem providing HTML, XML, SAX, and Reader parsers with XPath and CSS selector support.
gem "nokogiri"

# This library is intended to provide Ruby with an interface for validating JSON objects against a JSON schema conforming to JSON Schema Draft 4
gem "json-schema"

# Cron jobs in Ruby
gem "whenever"

gem "rails-i18n" # Centralization of locale data collection for Ruby on Rails

group :development, :test do
  # Call 'binding.pry' anywhere in the code to stop execution and get a debugger console
  gem "awesome_print" # Pretty print your Ruby objects with style -- in full color and with proper indentation
  gem "pry-byebug" # Step-by-step debugging and stack navigation in Pry
  gem "pry-rails" # Rails >= 3 pry initializer
  gem "standard" # Ruby Style Guide, with linter & automatic code fixer
  gem "stateoscope" # State Machine Visualizer - use like this: rake 'stateoscope:visualize[Workflow]'
  gem "knapsack" # Knapsack splits tests across CI nodes and makes sure that tests will run comparable time on each node.
end

group :development do
  # The Listen gem listens to file modifications and notifies you about the changes. Works everywhere!
  gem "listen"

  gem "binding_of_caller"

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem "spring"
  gem "spring-watcher-listen", "~> 2.0.0"

  gem "rubycritic", require: false # A Ruby code quality reporter
  gem "rubycritic-small-badge", require: false # Self service small badge for Rubycritic

  gem "bullet" # add https://github.com/flyerhzm/bullet as recommended by cancancan upgrade guide.
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem "capybara", ">= 2.15"
  gem "selenium-webdriver"
  gem "minitest-retry" # Re-run system test when the test fails.
  gem "capybara-lockstep" # Synchronize Capybara commands with application JavaScript and AJAX requests -> Avoids a lot of flaky tests

  # Group multiple assertions into one block to get results about all of them and not only the first failing. Inspired by rspec aggregate_failures. https://github.com/tjwp/aggregate_assertions
  gem "aggregate_assertions"

  # A gem providing "time travel", "time freezing", and "time acceleration" capabilities, making it simple to test time-dependent code.
  gem "timecop"

  # First gem that allows testing whenever-gem jobs
  gem "whenever-test"

  gem "simplecov", "~> 0.20.0", require: false # Code Coverage Reports

  gem "roo" # For reading and comparing Excel files in Export tests
end

# Simple HTTP and REST client for Ruby, we use it to access Tika Text extraction service
gem "rest-client"

# NewRelic application performance monitoring
gem "newrelic_rpm"

# mail templating
gem "mjml-rails"

# HTML-aware string truncation
gem "truncato"

# xlsx generation with charts, images, automated column width, customizable styles and full schema validation.
gem "caxlsx"
