#!/usr/bin/env ruby

def generate_compose_local
  erb = ERB.new(File.read("docker/operations/compose.dawn.yaml.erb"))
  erb.filename = "docker/operations/compose.dawn.yaml.erb"
  compse_local_content = erb.result(binding)
  File.write("docker/operations/compose.local.yaml", compse_local_content)
end

if defined?(namespace)
  namespace :deployment do
    desc "generate a compose.local.yaml from docker/operations/compose.dawn.yaml.erb"
    task :generate_compose_local do
      generate_compose_local
    end
  end
end

# Allow running this directly without rails
if __FILE__ == $0
  require "erb"
  generate_compose_local
end
