namespace :emails do
  def icon_candidates
    files = [Rails.root.join("app/models/event.rb")] + Dir.glob(Rails.root.join("app/models/events/*.rb"))
    icon_lines = []
    files.each do |file|
      icon_lines.concat(File.readlines(file).grep(/^\s*\{?icon: /))
    end
    unless icon_lines.length >= 10
      raise "There are only #{icon_lines.length} lines referencing icons in models/event.rb, but I expected at least 10."
    end

    icon_names = icon_lines.map { |line| line.strip.gsub(/^\s*\{?icon: "/, "").gsub(/"\}?$/, "") }
    non_mdi = icon_names - icon_names.grep(/^mdi-/)
    raise "Exected mdi icon names like mdi-foo, but got #{non_mdi.inspect}" if non_mdi.any?

    icon_names.uniq.sort
  end

  def filename(name)
    Rails.root.join("app/assets/images/mails/", "#{name}.png")
  end

  desc "Generate all images for usage in mails"
  task :generate_images do
    ### How to get icons usable in E-Mail?
    # 1. We could install e.g. yarn add @mdi/js ... but we don't need svgs (because they don't work in mails, see https://www.caniemail.com/search/?s=svg et al)
    # 2. We could convert the above-mentioned.
    # 3. We could see if we find what we need online -> works

    ### How to find the download URL manually:
    # 1. Go to https://materialdesignicons.com/icon/account-outline
    # 2. Choose advanced export at the bottom
    # 3. Set Size to 24 (which font size of the i.v-icon.mdi elements on /notifications)
    # 4. Padding 7 (24+7+7=38 -> 38x38 should be shown in the top right of the preview which is the width and height of div.v-timeline-item__dot)
    # 5. Click Circle (which sets corner radius to 19)
    # 6. foreground color #bdbdbd (which is the color of .v-application .grey--text.text--lighten-1)
    # 7. background color #f5f5f5 (which is from background of .v-timeline-item__inner-dot.grey.lighten-4)
    # 8. set foreground and background opacity to 1
    # 9. make sure browser dev tools are open, network tab, observe URL when clicking download button "Icon"
    # 10. note e.g. https://materialdesignicons.com/api/download/icon/png/79ECAAC6-71DC-406C-A9B7-57C5F0BAA9E0/24/bdbdbd/1/f5f5f5/1/19/7/account-outline as the result
    # 11. observe the url structure which currently is: /api/download/icon/FORMAT/GUID/SIZE/FOREGROUND/OPACITY/BACKGROUND/OPACITY/CORNERRADIUS/PADDING/CUSTOMNAME
    download_url = lambda { |name, guid|
      "https://materialdesignicons.com/api/download/icon/png/#{guid}/24/bdbdbd/1/f5f5f5/1/19/7/#{name}"
    }

    ### So, we  to get GUID from icon name somehow
    # 1. Go to https://materialdesignicons.com/, search one e.g. account-outline, open the details, get the perma-link at top of the modal
    # 2. Note the permalink, e.g. https://materialdesignicons.com/icon/account-outline
    # 3. That would have been too easy ;-)
    # 4. Observe which APIs are called in network tab. Interesting are:
    #    * The actual icon API at the very end: https://materialdesignicons.com/api/icon/79ECAAC6-71DC-406C-A9B7-57C5F0BAA9E0
    #    * The icon package which contains the icon: https://materialdesignicons.com/api/package/38EF63D0-4744-11E4-B3CF-842B2B6CFE1B?iconName=undefined
    #      * Unfortunately, the iconName param doesn't seem to do anything useful
    #    * The api entry point which leads us to the Icon Package: https://materialdesignicons.com/api/init
    init = JSON.parse(RestClient.get("https://materialdesignicons.com/api/init").body)
    package_id = init["packages"].first["id"] # we could also look for the name instead of the first...
    raise "package_id of materialdesignicons.com/api/init not in expected format." unless /\A[0-9A-F-]+\z/.match?(package_id)
    package = JSON.parse(RestClient.get("https://materialdesignicons.com/api/package/#{package_id}?iconName=undefined").body)
    guid_lookup = package["icons"].map { |icon| [icon["name"], icon["id"]] }.to_h

    guid = lambda { |name|
      guid_lookup[name.delete_prefix("mdi-")] || raise("Could not find GUID for icon #{name}. Maybe extend lookup to use aliases etc.")
    }

    # Do the actual download
    icon_candidates.each do |name|
      puts "⬇️  Downloading #{name}"
      File.write(filename(name), RestClient.get(download_url.call(name, guid.call(name))).body, mode: "wb")
      # Alternative: `wget -O #{filename(name)} #{download_url.call(name, guid.call(name))}`
    end
    puts "👍  All image files needed for emails downloaded. You can now commit them."
  end

  desc "Test if images are available for usage in mails"
  task :ensure_images do
    files = icon_candidates.map { |name| filename(name) }
    missing = files.reject { |name| File.exist?(name) }
    if missing.any?
      puts "🛑  Missing the following files:\n#{missing.join("\n")}"
      puts "ℹ️  try rails emails:generate_images or see lib/tasks/emails.rake if that fails."
      raise "Missing avatar images for emails. See above."
    else
      puts "👍  All image files needed for emails found"
    end
  end
end
